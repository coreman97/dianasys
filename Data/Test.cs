﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Entities;

namespace DianaSys.Data
{
    public class Test
    {
        public readonly bddianasis_onlineContext _context;

        public Test(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }

        public IEnumerable<Empresas> GetEmpresas()
        {
            return _context.Empresas.ToList();
        }
    }
}
