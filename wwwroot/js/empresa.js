﻿if (localStorage.getItem("isFromLogin")) {
    var hide_table = true;
    var empresas_list = [];
    // Al abrir
    $(() => {
        enableButtons(INICIO)
        $("#loading").css('display', 'none');
        CargarEmpresas();
        CargarTiposPuc();
        BloquearCampos(0)
        $("#busqueda").keyup(function () {
            var value = this.value.toLowerCase().trim();
            $("table tr").each(function (index) {
                if (!index) return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });

        $("#busqueda1").keyup(function () {
            var value = this.value.toLowerCase().trim();
            $("table tr").each(function (index) {
                if (!index) return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });
    });

    function CargarEmpresas() {
        $.ajax({
            method: "GET",
            url: "empresas/list/" + localStorage.getItem("asociado")
        }).done(empresas => {
            console.log(empresas);
            $('#table-content tr').remove();
            for (empresa of empresas) {
                $('#table-content').append(`
                    <tr ondblclick="CargarEmpresaPorId(this.id)" id="${empresa.empIdStCodigo}" class="border-b">
                        <td>${empresa.empIdStCodigo}</td>
                        <td>${empresa.empStNit}</td>
                        <td>${empresa.empStNombre}</td>
                        <td>${empresa.empStEstado}</td>
                    </tr>
                 `)
            }
        });
    }

    function CargarTiposPuc() {
        $.ajax({
            method: "GET",
            url: "empresas/tipospuc"
        }).done(pucs => {
            $('#table-content tr').remove();
            for (puc of pucs) {
                $('#puc_list').append(`
                    <option value="${puc.tpuIdStCodigo}">${puc.tpuStNombre}</option>
                 `)
            }
        });
    }

    function GenerarCartilla() {
        VerificarCartilla();
    }

    $('#puc').change(() => {
        confirmAlert("Desea generar la cartilla?", "Atención", GenerarCartilla);
    });

    function CargarEmpresaPorId(empresaId) {
        $.ajax({
            method: 'GET',
            url: "empresas/" + empresaId + "/" + localStorage.getItem("asociado")
        }).done(empresa => {
            $('#EmpIdInAsociado').val(localStorage.getItem("asociado"))
            $('#EmpIdStCodigo').val(empresa.empIdStCodigo)
            $('#EmpStNit').val(empresa.empStNit)
            $('#EmpStNombre').val(empresa.empStNombre)
            $('#EmpStDireccion').val(empresa.empStDireccion),
                $('#EmpStEstado').val(empresa.empStEstado)
            $('#EmpStSigla').val(empresa.empStSigla)
            $('#EmpStAbreviatura').val(empresa.empStAbreviatura)
            $('#EmpIdStTipoEmpresa').val(empresa.empIdStTipoEmpresa)
            $('#EmpStObjetoSocial').val(empresa.empStObjetoSocial)
            $('#EmpDtFechaConstitucion').val(empresa.empDtFechaConstitucion.slice(0, empresa.empDtFechaConstitucion.indexOf('T')))
            $('#EmpStMatriculaMercantil').val(empresa.empStMatriculaMercantil)
            $('#EmpStNotaria').val(empresa.empStNotaria)
            $('#EmpStNumeroEscritura').val(empresa.empStNumeroEscritura)
            $('#EmpDtFechaRegistro').val(empresa.empDtFechaRegistro.slice(0, empresa.empDtFechaRegistro.indexOf('T')))
            $('#EmpStLibroRegistro').val(empresa.empStLibroRegistro)
            $('#EmpInDuracion').val(empresa.empInDuracion)
            $('#empIdInPaisLocalizacion').val(empresa.empIdInPaisLocalizacion)
            $('#empIdInDptoLocalizacion').val(empresa.empIdInDptoLocalizacion)
            $('#empIdInMuniLocalizacion').val(empresa.empIdInMuniLocalizacion)
            $('#tbl_buscar').css('display', 'none')
            BuscarUbicacion();
        });
        BloquearCampos(0);
        enableButtons(BUSCAR);
    }

    function CargarMunicipios() {
        let municipiosList = $('#AflIdinMunicipioLocalizacion_list');
        $.ajax({
            method: 'GET',
            url: "municipios"
        }).done(data => {
            for (d of data) {
                municipiosList.append(`<option value="${d.munIdInCodigo}">${d.munStNombre}</option>`)
            }
        });
    }

    function ObtenerConsecutivo() {
        $.ajax({
            method: 'GET',
            url: "empresas/consecutivo/" + localStorage.getItem("asociado")
        }).done(consecutivo => {
            var consec = consecutivo;
            if (consec.length == 1) {
                consec = "0" + consec;
            }
            $('#EmpIdStCodigo').val(consec)
        })
    }

    function limpiar_formulario() {
        $('#EmpIdInAsociado').val(localStorage.getItem("asociado"))
        $('#EmpIdStCodigo').val("")
        $('#EmpStNit').val("")
        $('#EmpStNombre').val("")
        $('#EmpStDireccion').val("")
        $('#EmpStEstado').val("")
        $('#EmpStSigla').val("")
        $('#EmpStAbreviatura').val("")
        $('#EmpIdStTipoEmpresa').val("")
        $('#EmpStObjetoSocial').val("")
        $('#EmpDtFechaConstitucion').val("")
        $('#EmpStMatriculaMercantil').val("")
        $('#EmpStNotaria').val("")
        $('#EmpStNumeroEscritura').val("")
        $('#EmpDtFechaRegistro').val("")
        $('#EmpStLibroRegistro').val("")
        $('#EmpInDuracion').val("")
        $("#puc").val("")
        $('#empIdInPaisLocalizacion').val("")
        $('#empIdInDptoLocalizacion').val("")
        $('#empIdInMuniLocalizacion').val("")
        $("#puc").prop('disabled', true);
        $('#munStNombre').val("")
    }

    function BloquearCampos(Estado) {
        if (Estado == 1) {
            $('#EmpStNit').prop('disabled', false);
            $('#EmpStNombre').prop('disabled', false);
            $('#EmpStDireccion').prop('disabled', false);
            $('#EmpStEstado').prop('disabled', false);
            $('#EmpStSigla').prop('disabled', false);
            $('#EmpStAbreviatura').prop('disabled', false);
            $('#EmpIdStTipoEmpresa').prop('disabled', false);
            $('#EmpStObjetoSocial').prop('disabled', false);
            $('#EmpDtFechaConstitucion').prop('disabled', false);
            $('#EmpStMatriculaMercantil').prop('disabled', false);
            $('#EmpStNotaria').prop('disabled', false);
            $('#EmpStNumeroEscritura').prop('disabled', false);
            $('#EmpDtFechaRegistro').prop('disabled', false);
            $('#EmpStLibroRegistro').prop('disabled', false);
            $('#EmpInDuracion').prop('disabled', false);
            $('#btn_ubicacion').attr('disabled', false);
        }
        else {
            $('#EmpIdInAsociado').prop('disabled', true);
            $('#EmpIdStCodigo').prop('disabled', true);
            $('#EmpStNit').prop('disabled', true);
            $('#EmpStNombre').prop('disabled', true);
            $('#EmpStDireccion').prop('disabled', true);
            $('#EmpStEstado').prop('disabled', true);
            $('#EmpStSigla').prop('disabled', true);
            $('#EmpStAbreviatura').prop('disabled', true);
            $('#EmpIdStTipoEmpresa').prop('disabled', true);
            $('#EmpStObjetoSocial').prop('disabled', true);
            $('#EmpDtFechaConstitucion').prop('disabled', true);
            $('#EmpStMatriculaMercantil').prop('disabled', true);
            $('#EmpStNotaria').prop('disabled', true);
            $('#EmpStNumeroEscritura').prop('disabled', true);
            $('#EmpDtFechaRegistro').prop('disabled', true);
            $('#EmpStLibroRegistro').prop('disabled', true);
            $('#EmpInDuracion').prop('disabled', true);
            $('#empIdInPaisLocalizacion').prop('disabled', true);
            $('#empIdInDptoLocalizacion').prop('disabled', true);
            $('#empIdInMuniLocalizacion').prop('disabled', true);
            $('#btn_ubicacion').attr('disabled', true);
        }
    }
    function Guardar() {
        if (ValidarCampos()) {
            var empresa = {
                EmpIdInAsociado: $('#EmpIdInAsociado').val(),
                EmpIdStCodigo: $('#EmpIdStCodigo').val(),
                EmpStNit: $('#EmpStNit').val(),
                EmpStNombre: $('#EmpStNombre').val(),
                EmpStDireccion: $('#EmpStDireccion').val(),
                EmpIdInPaisLocalizacion: $('#empIdInPaisLocalizacion').val(), // TODO:  Debe ser seleccionable, falta crear campo.
                EmpIdInDptoLocalizacion: $('#empIdInDptoLocalizacion').val(),
                EmpIdInMuniLocalizacion: $('#empIdInMuniLocalizacion').val(),
                EmpStEstado: $('#EmpStEstado').val(),
                EmpStSigla: $('#EmpStSigla').val(),
                EmpStAbreviatura: $('#EmpStAbreviatura').val(),
                EmpIdStTipoEmpresa: $('#EmpIdStTipoEmpresa').val(),
                EmpStObjetoSocial: $('#EmpStObjetoSocial').val(),
                EmpDtFechaConstitucion: $('#EmpDtFechaConstitucion').val(),
                EmpStMatriculaMercantil: $('#EmpStMatriculaMercantil').val(),
                EmpStNotaria: $('#EmpStNotaria').val(),
                EmpStNumeroEscritura: $('#EmpStNumeroEscritura').val(),
                EmpDtFechaRegistro: $('#EmpDtFechaRegistro').val(),
                EmpStLibroRegistro: $('#EmpStLibroRegistro').val(),
                EmpInDuracion: $('#EmpInDuracion').val(),
                EmpIdStUsuario: localStorage.getItem("usuario")
            }
            $.ajax({
                method: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: "empresas/guardar",
                data: JSON.stringify(empresa)
            }).done(() => {
                limpiar_formulario();
                customAlert("Se ha guardado con exito.", "Exito");
                CargarEmpresas();
                enableButtons(GUARDAR);
                BloquearCampos(0);

                if (localStorage.getItem("isFromLogin")) {
                    localStorage.setItem("isFromLogin", false);
                    location.href = "/autenticacion";
                }
            });
        }

    }

    function eliminar(codigo) {
        $.ajax({
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "empresas/" + codigo + "/" + localStorage.getItem("asociado")
        }).done(() => {
            limpiar_formulario();
            CargarEmpresas();
            customAlert("El registro se elimino correctamente.", "Atención");
            enableButtons(ELIMINAR);
        });
    }

    function añadir() {
        limpiar_formulario();
        ObtenerConsecutivo();
        BloquearCampos(1);
        enableButtons(NUEVO);
    }

    function enableButtons(button) {
        switch (button) {
            case 0: // inicial
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_guardar').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300');
                $("#puc").prop('disabled', true);
                break;
            case 1: //nuevo
                $('#btn_guardar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_limpiar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', true).removeClass('bg-red-600').addClass('bg-red-300');
                $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_añadir').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                break;
            case 2: // guardar.
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', true).addClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
            case 3: // Deshacer
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                break;
            case 4: // editar
                $('#btn_guardar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_limpiar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_añadir').attr('disabled', false).removeClass('bg-blue-600').addClass('bg-blue-300');

                break;
            case 5: // Eliminar
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                break;
            case 6: // buscar
                $('#btn_edit').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', false).removeClass('bg-red-300').addClass('bg-red-600');
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#agregar_puc').attr('disabled', false).addClass('bg-blue-600');
                break;
        }
    }

    function ValidarCampos() {
        if ($('#EmpStNit').val() == "" || $('#EmpStNit').val() == null) {
            customAlert("Señor usuario digite el nit", "Atención");
            $('#EmpStNit').focus();
            return false;
        }

        if ($('#EmpStNombre').val() == "" || $('#EmpStNombre').val() == null) {
            customAlert("Señor usuario digite el nombre de la empresa", "Atención");
            $('#EmpStNombre').focus();
            return false;
        }

        if ($('#EmpStSigla').val() == "") {
            customAlert("Señor usuario digite la sigla de la empresa", "Atención");
            $('#EmpStSigla').focus();
            return false;
        }
        if ($('#EmpStDireccion').val() == "") {
            customAlert("Señor usuario digite la dirección", "Atención");
            $('#EmpStDireccion').focus();
            return false;
        }

        if ($('#empIdInPaisLocalizacion').val() == "" || $('#EmpIdInDptoLocalizacion').val() == "" || $('#empIdInMuniLocalizacion').val() == "") {
            customAlert("Señor usuario seleccione la localización geografica", "Atención");
            $('#AflIdinMunicipioLocalizacion').focus();
            return false;
        }

        if ($('#EmpIdStTipoEmpresa').val() == "" || $('#EmpIdStTipoEmpresa').val() == null) {
            customAlert("Señor usuario seleccione el tipo de empresa", "Atención");
            $('#EmpIdStTipoEmpresa').focus();
            return false;
        }

        if ($('#EmpStObjetoSocial').val() == "" || $('#EmpStObjetoSocial').val() == null) {
            customAlert("Señor usuario digite el objeto social", "Atención");
            $('#EmpStObjetoSocial').focus();
            return false;
        }

        if ($('#EmpDtFechaConstitucion').val() == "" || $('#EmpDtFechaConstitucion').val() == null) {
            customAlert("Señor usuario seleccione la fecha de constitución", "Atención");
            $('#EmpDtFechaConstitucion').focus();
            return false;
        }

        if ($('#EmpStNotaria').val() == "") {
            customAlert("Señor usuario digite la notaria", "Atención");
            $('#EmpStNotaria').focus();
            return false;
        }

        if ($('#EmpStNumeroEscritura').val() == "") {
            customAlert("Señor usuario digite el numero de la escritura", "Atención");
            $('#EmpStNumeroEscritura').focus();
            return false;
        }

        if ($('#EmpDtFechaRegistro').val() == "" || $('#EmpDtFechaRegistro').val() == null) {
            customAlert("Señor usuario seleccione la fecha de registro", "Atención");
            $('#EmpDtFechaRegistro').focus();
            return false;
        }

        if ($('#EmpStLibroRegistro').val() == "") {
            customAlert("Señor usuario digite el libro de registro", "Atención");
            $('#EmpStLibroRegistro').focus();
            return false;
        }

        if ($('#EmpInDuracion').val() == "") {
            customAlert("Señor usuario digite la duración en años", "Atención");
            $('#EmpInDuracion').focus();
            return false;
        }

        if ($('#EmpStEstado').val() == "" || $('#EmpStEstado').val() == null) {
            customAlert("Señor usuario seleccione el estado", "Atención");
            $('#EmpStEstado').focus();
            return false;
        }

        return true;
    }

    function VerificarCartilla() {
        $.ajax({
            method: 'GET',
            url: "empresas/VerificarCartilla/" + $('#EmpIdStCodigo').val() + "/" + localStorage.getItem("asociado")
        }).done(data => {
            if (data) {
                customAlert("Señor usuario la empresa tiene plan de cuentas asignada", "Atención");
                $('#puc').val("");
                $("#puc").prop('disabled', true);
                return false
            }
            else {
                $("#loading").css('display', 'block');
                $.ajax({
                    method: "GET",
                    url: "empresas/cartilla/" + localStorage.getItem("asociado") + "/" + $('#EmpIdStCodigo').val() + "/" + localStorage.getItem("usuario") + "/" + $('#puc').val()
                }).done(seCreo => {
                    if (seCreo) {
                        customAlert("El plan de cuentas se genero correctamente.", "Accion Exitosa")
                        limpiar_formulario();
                        BloquearCampos(0)
                        enableButtons(GUARDAR);
                    } else {
                        customAlert("Ha ocurrido un error al ejecutar el proceso.", "Error")
                    }
                    $("#loading").css('display', 'none');
                })
            }
        })

    }

    function CargarUbicacion() {
        fetch("Municipios/Listado", {
            method: "GET", headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response =>
                response.json()
            )
            .then(data => {
                $('#tbl_ubicacion1 tr').remove();
                for (d of data) {
                    $('#tbl_ubicacion1').append(`
                                <tr ondblclick="MostarUbicacion(${d.munIdInPais},${d.munIdInDpto},${d.munIdInCodigo})" class="border-b">
                                    <td>${d.munStNombre}</td>
                                    <td>${d.dptStNombre}</td>
                                    <td>${d.paiStNombre}</td>
                                </tr>
                            `)
                }


            });
    }

    function MostarUbicacion(munIdInPais, munIdInDpto, munIdInCodigo) {
        $.ajax({
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "Municipios/Buscar/" + munIdInCodigo + " / " + munIdInPais + " / " + munIdInDpto,
        }).done(data => {
            $('#empIdInMuniLocalizacion').val(data.munIdInCodigo)
            $('#munStNombre').val(data.munStNombre)
            $('#empIdInPaisLocalizacion').val(data.munIdInPais)
            $('#empIdInDptoLocalizacion').val(data.munIdInDpto)
            $('#tbl_ubicacion').css('display', 'none')
        });

    }

    function BuscarUbicacion() {
        munIdInPais = $('#empIdInPaisLocalizacion').val();
        munIdInDpto = $('#empIdInDptoLocalizacion').val();
        munIdInCodigo = $('#empIdInMuniLocalizacion').val();
        $.ajax({
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "Municipios/Buscar/" + munIdInCodigo + " / " + munIdInPais + " / " + munIdInDpto,
        }).done(data => {
            $('#munStNombre').val(data.munStNombre)

        });

    }
    // MODAL
    $('#btn_buscar').click(() => {
        CargarEmpresas();
        $('#tbl_buscar').css('display', 'block');
    });
    $('.close').click(() => $('#tbl_buscar').css('display', 'none'))

    $('#btn_limpiar').click(() => {
        añadir();
        enableButtons(CANCELAR);
        BloquearCampos(0);
    });
    $('#btn_añadir').click(() => {
        añadir();
    });
    $('#agregar_puc').click(() => {
        $("#puc").prop('disabled', false);
    });

    $('#btn_guardar').click(() => {
        Guardar();
    });

    $('#btn_edit').click(() => {
        enableButtons(EDITAR);
        BloquearCampos(1);
    })

    $('#btn_delete').click(() => {
        var codigo = $('#EmpIdStCodigo').val();
        confirmAlert("Sr. Usuario esta seguro que desea eliminar este registro?", "Atención", eliminar, codigo);
    });

    $("#btn_ubicacion").click(() => {
        CargarUbicacion();
        $("#tbl_ubicacion").css('display', 'block');
    });
    $('.close').click(() => $('#tbl_ubicacion').css('display', 'none'))
} else {
    var hide_table = true;
    var empresas_list = [];
    // Al abrir
    $(() => {
        enableButtons(INICIO)
        $("#loading").css('display', 'none');
        CargarEmpresas();
        CargarTiposPuc();
        BloquearCampos(0)
        $("#busqueda").keyup(function () {
            var value = this.value.toLowerCase().trim();
            $("table tr").each(function (index) {
                if (!index) return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });

        $("#busqueda1").keyup(function () {
            var value = this.value.toLowerCase().trim();
            $("table tr").each(function (index) {
                if (!index) return;
                $(this).find("td").each(function () {
                    var id = $(this).text().toLowerCase().trim();
                    var not_found = (id.indexOf(value) == -1);
                    $(this).closest('tr').toggle(!not_found);
                    return not_found;
                });
            });
        });
    });

    function CargarEmpresas() {
        $.ajax({
            method: "GET",
            url: "Contabilidad/empresas/list/" + ASOCIADO
        }).done(empresas => {
            console.log(empresas);
            $('#table-content tr').remove();
            for (empresa of empresas) {
                $('#table-content').append(`
                    <tr ondblclick="CargarEmpresaPorId(this.id)" id="${empresa.empIdStCodigo}" class="border-b">
                        <td>${empresa.empIdStCodigo}</td>
                        <td>${empresa.empStNit}</td>
                        <td>${empresa.empStNombre}</td>
                        <td>${empresa.empStEstado}</td>
                    </tr>
                 `)
            }
        });
    }

    function CargarTiposPuc() {
        $.ajax({
            method: "GET",
            url: "Contabilidad/empresas/tipospuc"
        }).done(pucs => {
            $('#table-content tr').remove();
            for (puc of pucs) {
                $('#puc_list').append(`
                    <option value="${puc.tpuIdStCodigo}">${puc.tpuStNombre}</option>
                 `)
            }
        });
    }

    function GenerarCartilla() {
        if (VerificarCartilla()) {
            $("#loading").css('display', 'block');
            $.ajax({
                method: "GET",
                url: "Contabilidad/empresas/cartilla/" + ASOCIADO + "/" + $('#EmpIdStCodigo').val() + "/" + USUARIO + "/" + $('#puc').val()
            }).done(seCreo => {
                if (seCreo) {
                    customAlert("El plan de cuentas se genero correctamente.", "Accion Exitosa")
                    limpiar_formulario();
                    BloquearCampos(0)
                    enableButtons(GUARDAR);
                } else {
                    customAlert("Ha ocurrido un error al ejecutar el proceso.", "Error")
                }
                $("#loading").css('display', 'none');
            })
        }

    }

    $('#puc').change(() => {
        confirmAlert("Desea generar la cartilla?", "Atención", GenerarCartilla);
    });

    function CargarEmpresaPorId(empresaId) {
        $.ajax({
            method: 'GET',
            url: "Contabilidad/empresas/" + empresaId + "/" + ASOCIADO
        }).done(empresa => {
            $('#EmpIdInAsociado').val(ASOCIADO)
            $('#EmpIdStCodigo').val(empresa.empIdStCodigo)
            $('#EmpStNit').val(empresa.empStNit)
            $('#EmpStNombre').val(empresa.empStNombre)
            $('#EmpStDireccion').val(empresa.empStDireccion),
                $('#EmpStEstado').val(empresa.empStEstado)
            $('#EmpStSigla').val(empresa.empStSigla)
            $('#EmpStAbreviatura').val(empresa.empStAbreviatura)
            $('#EmpIdStTipoEmpresa').val(empresa.empIdStTipoEmpresa)
            $('#EmpStObjetoSocial').val(empresa.empStObjetoSocial)
            $('#EmpDtFechaConstitucion').val(empresa.empDtFechaConstitucion.slice(0, empresa.empDtFechaConstitucion.indexOf('T')))
            $('#EmpStMatriculaMercantil').val(empresa.empStMatriculaMercantil)
            $('#EmpStNotaria').val(empresa.empStNotaria)
            $('#EmpStNumeroEscritura').val(empresa.empStNumeroEscritura)
            $('#EmpDtFechaRegistro').val(empresa.empDtFechaRegistro.slice(0, empresa.empDtFechaRegistro.indexOf('T')))
            $('#EmpStLibroRegistro').val(empresa.empStLibroRegistro)
            $('#EmpInDuracion').val(empresa.empInDuracion)
            $('#empIdInPaisLocalizacion').val(empresa.empIdInPaisLocalizacion)
            $('#empIdInDptoLocalizacion').val(empresa.empIdInDptoLocalizacion)
            $('#empIdInMuniLocalizacion').val(empresa.empIdInMuniLocalizacion)
            $('#tbl_buscar').css('display', 'none')
            BuscarUbicacion();
        });
        BloquearCampos(0);
        enableButtons(BUSCAR);
    }

    function CargarMunicipios() {
        let municipiosList = $('#AflIdinMunicipioLocalizacion_list');
        $.ajax({
            method: 'GET',
            url: "Contabilidad/municipios"
        }).done(data => {
            for (d of data) {
                municipiosList.append(`<option value="${d.munIdInCodigo}">${d.munStNombre}</option>`)
            }
        });
    }

    function ObtenerConsecutivo() {
        $.ajax({
            method: 'GET',
            url: "Contabilidad/empresas/consecutivo/" + ASOCIADO
        }).done(consecutivo => {
            var consec = consecutivo;
            if (consec.length == 1) {
                consec = "0" + consec;
            }
            $('#EmpIdStCodigo').val(consec)
        })
    }

    function limpiar_formulario() {
        $('#EmpIdInAsociado').val(ASOCIADO)
        $('#EmpIdStCodigo').val("")
        $('#EmpStNit').val("")
        $('#EmpStNombre').val("")
        $('#EmpStDireccion').val("")
        $('#EmpStEstado').val("")
        $('#EmpStSigla').val("")
        $('#EmpStAbreviatura').val("")
        $('#EmpIdStTipoEmpresa').val("")
        $('#EmpStObjetoSocial').val("")
        $('#EmpDtFechaConstitucion').val("")
        $('#EmpStMatriculaMercantil').val("")
        $('#EmpStNotaria').val("")
        $('#EmpStNumeroEscritura').val("")
        $('#EmpDtFechaRegistro').val("")
        $('#EmpStLibroRegistro').val("")
        $('#EmpInDuracion').val("")
        $("#puc").val("")
        $('#empIdInPaisLocalizacion').val("")
        $('#empIdInDptoLocalizacion').val("")
        $('#empIdInMuniLocalizacion').val("")
        $("#puc").prop('disabled', true);
        $('#munStNombre').val("")
    }

    function BloquearCampos(Estado) {
        if (Estado == 1) {
            $('#EmpStNit').prop('disabled', false);
            $('#EmpStNombre').prop('disabled', false);
            $('#EmpStDireccion').prop('disabled', false);
            $('#EmpStEstado').prop('disabled', false);
            $('#EmpStSigla').prop('disabled', false);
            $('#EmpStAbreviatura').prop('disabled', false);
            $('#EmpIdStTipoEmpresa').prop('disabled', false);
            $('#EmpStObjetoSocial').prop('disabled', false);
            $('#EmpDtFechaConstitucion').prop('disabled', false);
            $('#EmpStMatriculaMercantil').prop('disabled', false);
            $('#EmpStNotaria').prop('disabled', false);
            $('#EmpStNumeroEscritura').prop('disabled', false);
            $('#EmpDtFechaRegistro').prop('disabled', false);
            $('#EmpStLibroRegistro').prop('disabled', false);
            $('#EmpInDuracion').prop('disabled', false);
            $('#btn_ubicacion').attr('disabled', false);
        }
        else {
            $('#EmpIdInAsociado').prop('disabled', true);
            $('#EmpIdStCodigo').prop('disabled', true);
            $('#EmpStNit').prop('disabled', true);
            $('#EmpStNombre').prop('disabled', true);
            $('#EmpStDireccion').prop('disabled', true);
            $('#EmpStEstado').prop('disabled', true);
            $('#EmpStSigla').prop('disabled', true);
            $('#EmpStAbreviatura').prop('disabled', true);
            $('#EmpIdStTipoEmpresa').prop('disabled', true);
            $('#EmpStObjetoSocial').prop('disabled', true);
            $('#EmpDtFechaConstitucion').prop('disabled', true);
            $('#EmpStMatriculaMercantil').prop('disabled', true);
            $('#EmpStNotaria').prop('disabled', true);
            $('#EmpStNumeroEscritura').prop('disabled', true);
            $('#EmpDtFechaRegistro').prop('disabled', true);
            $('#EmpStLibroRegistro').prop('disabled', true);
            $('#EmpInDuracion').prop('disabled', true);
            $('#empIdInPaisLocalizacion').prop('disabled', true);
            $('#empIdInDptoLocalizacion').prop('disabled', true);
            $('#empIdInMuniLocalizacion').prop('disabled', true);
            $('#btn_ubicacion').attr('disabled', true);
        }
    }
    function Guardar() {
        if (ValidarCampos()) {
            var empresa = {
                EmpIdInAsociado: $('#EmpIdInAsociado').val(),
                EmpIdStCodigo: $('#EmpIdStCodigo').val(),
                EmpStNit: $('#EmpStNit').val(),
                EmpStNombre: $('#EmpStNombre').val(),
                EmpStDireccion: $('#EmpStDireccion').val(),
                EmpIdInPaisLocalizacion: $('#empIdInPaisLocalizacion').val(), // TODO:  Debe ser seleccionable, falta crear campo.
                EmpIdInDptoLocalizacion: $('#empIdInDptoLocalizacion').val(),
                EmpIdInMuniLocalizacion: $('#empIdInMuniLocalizacion').val(),
                EmpStEstado: $('#EmpStEstado').val(),
                EmpStSigla: $('#EmpStSigla').val(),
                EmpStAbreviatura: $('#EmpStAbreviatura').val(),
                EmpIdStTipoEmpresa: $('#EmpIdStTipoEmpresa').val(),
                EmpStObjetoSocial: $('#EmpStObjetoSocial').val(),
                EmpDtFechaConstitucion: $('#EmpDtFechaConstitucion').val(),
                EmpStMatriculaMercantil: $('#EmpStMatriculaMercantil').val(),
                EmpStNotaria: $('#EmpStNotaria').val(),
                EmpStNumeroEscritura: $('#EmpStNumeroEscritura').val(),
                EmpDtFechaRegistro: $('#EmpDtFechaRegistro').val(),
                EmpStLibroRegistro: $('#EmpStLibroRegistro').val(),
                EmpInDuracion: $('#EmpInDuracion').val(),
                EmpIdStUsuario: USUARIO
            }
            $.ajax({
                method: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: "Contabilidad/empresas/guardar",
                data: JSON.stringify(empresa)
            }).done(() => {
                limpiar_formulario();
                customAlert("Se ha guardado con exito.", "Exito");
                CargarEmpresas();
                enableButtons(GUARDAR);
                BloquearCampos(0);
            });
        }

    }

    function eliminar(codigo) {
        $.ajax({
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "Contabilidad/empresas/" + codigo + "/" + ASOCIADO
        }).done(() => {
            limpiar_formulario();
            CargarEmpresas();
            customAlert("El registro se elimino correctamente.", "Atención");
            enableButtons(ELIMINAR);
        });
    }

    function añadir() {
        limpiar_formulario();
        ObtenerConsecutivo();
        BloquearCampos(1);
        enableButtons(NUEVO);
    }

    function enableButtons(button) {
        switch (button) {
            case 0: // inicial
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_guardar').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300');
                $("#puc").prop('disabled', true);
                break;
            case 1: //nuevo
                $('#btn_guardar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_limpiar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', true).removeClass('bg-red-600').addClass('bg-red-300');
                $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_añadir').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                break;
            case 2: // guardar.
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', true).addClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
            case 3: // Deshacer
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                $('#agregar_puc').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                break;
            case 4: // editar
                $('#btn_guardar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_limpiar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_añadir').attr('disabled', false).removeClass('bg-blue-600').addClass('bg-blue-300');

                break;
            case 5: // Eliminar
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
                $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
                $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
                $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
                break;
            case 6: // buscar
                $('#btn_edit').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
                $('#btn_delete').attr('disabled', false).removeClass('bg-red-300').addClass('bg-red-600');
                $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
                $('#agregar_puc').attr('disabled', false).addClass('bg-blue-600');
                break;
        }
    }

    function ValidarCampos() {
        if ($('#EmpStNit').val() == "" || $('#EmpStNit').val() == null) {
            customAlert("Señor usuario digite el nit", "Atención");
            $('#EmpStNit').focus();
            return false;
        }

        if ($('#EmpStNombre').val() == "" || $('#EmpStNombre').val() == null) {
            customAlert("Señor usuario digite el nombre de la empresa", "Atención");
            $('#EmpStNombre').focus();
            return false;
        }

        if ($('#EmpStSigla').val() == "") {
            customAlert("Señor usuario digite la sigla de la empresa", "Atención");
            $('#EmpStSigla').focus();
            return false;
        }
        if ($('#EmpStDireccion').val() == "") {
            customAlert("Señor usuario digite la dirección", "Atención");
            $('#EmpStDireccion').focus();
            return false;
        }

        if ($('#empIdInPaisLocalizacion').val() == "" || $('#EmpIdInDptoLocalizacion').val() == "" || $('#empIdInMuniLocalizacion').val() == "") {
            customAlert("Señor usuario seleccione la localización geografica", "Atención");
            $('#AflIdinMunicipioLocalizacion').focus();
            return false;
        }

        if ($('#EmpIdStTipoEmpresa').val() == "" || $('#EmpIdStTipoEmpresa').val() == null) {
            customAlert("Señor usuario seleccione el tipo de empresa", "Atención");
            $('#EmpIdStTipoEmpresa').focus();
            return false;
        }

        if ($('#EmpStObjetoSocial').val() == "" || $('#EmpStObjetoSocial').val() == null) {
            customAlert("Señor usuario digite el objeto social", "Atención");
            $('#EmpStObjetoSocial').focus();
            return false;
        }

        if ($('#EmpDtFechaConstitucion').val() == "" || $('#EmpDtFechaConstitucion').val() == null) {
            customAlert("Señor usuario seleccione la fecha de constitución", "Atención");
            $('#EmpDtFechaConstitucion').focus();
            return false;
        }

        if ($('#EmpStNotaria').val() == "") {
            customAlert("Señor usuario digite la notaria", "Atención");
            $('#EmpStNotaria').focus();
            return false;
        }

        if ($('#EmpStNumeroEscritura').val() == "") {
            customAlert("Señor usuario digite el numero de la escritura", "Atención");
            $('#EmpStNumeroEscritura').focus();
            return false;
        }

        if ($('#EmpDtFechaRegistro').val() == "" || $('#EmpDtFechaRegistro').val() == null) {
            customAlert("Señor usuario seleccione la fecha de registro", "Atención");
            $('#EmpDtFechaRegistro').focus();
            return false;
        }

        if ($('#EmpStLibroRegistro').val() == "") {
            customAlert("Señor usuario digite el libro de registro", "Atención");
            $('#EmpStLibroRegistro').focus();
            return false;
        }

        if ($('#EmpInDuracion').val() == "") {
            customAlert("Señor usuario digite la duración en años", "Atención");
            $('#EmpInDuracion').focus();
            return false;
        }

        if ($('#EmpStEstado').val() == "" || $('#EmpStEstado').val() == null) {
            customAlert("Señor usuario seleccione el estado", "Atención");
            $('#EmpStEstado').focus();
            return false;
        }

        return true;
    }

    function VerificarCartilla() {
        $.ajax({
            method: 'GET',
            url: "Contabilidad/empresas/VerificarCartilla/" + $('#EmpIdStCodigo').val() + "/" + ASOCIADO
        }).done(data => {
            alert(data)
            if (data) {
                customAlert("Señor usuario la empresa tiene plan de cuentas asignada", "Atención");
                $('#puc').val("");
                $("#puc").prop('disabled', true);
                return false
            }
            else { return true }
        })

    }

    function CargarUbicacion() {
        fetch("Contabilidad/Municipios/Listado", {
            method: "GET", headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response =>
                response.json()
            )
            .then(data => {
                $('#tbl_ubicacion1 tr').remove();
                for (d of data) {
                    $('#tbl_ubicacion1').append(`
                                <tr ondblclick="MostarUbicacion(${d.munIdInPais},${d.munIdInDpto},${d.munIdInCodigo})" class="border-b">
                                    <td>${d.munStNombre}</td>
                                    <td>${d.dptStNombre}</td>
                                    <td>${d.paiStNombre}</td>
                                </tr>
                            `)
                }


            });
    }

    function MostarUbicacion(munIdInPais, munIdInDpto, munIdInCodigo) {
        $.ajax({
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "Contabilidad/Municipios/Buscar/" + munIdInCodigo + " / " + munIdInPais + " / " + munIdInDpto,
        }).done(data => {
            $('#empIdInMuniLocalizacion').val(data.munIdInCodigo)
            $('#munStNombre').val(data.munStNombre)
            $('#empIdInPaisLocalizacion').val(data.munIdInPais)
            $('#empIdInDptoLocalizacion').val(data.munIdInDpto)
            $('#tbl_ubicacion').css('display', 'none')
        });

    }

    function BuscarUbicacion() {
        munIdInPais = $('#empIdInPaisLocalizacion').val();
        munIdInDpto = $('#empIdInDptoLocalizacion').val();
        munIdInCodigo = $('#empIdInMuniLocalizacion').val();
        $.ajax({
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
            },
            url: "Contabilidad/Municipios/Buscar/" + munIdInCodigo + " / " + munIdInPais + " / " + munIdInDpto,
        }).done(data => {
            $('#munStNombre').val(data.munStNombre)

        });

    }
    // MODAL
    $('#btn_buscar').click(() => {
        CargarEmpresas();
        $('#tbl_buscar').css('display', 'block');
    });
    $('.close').click(() => $('#tbl_buscar').css('display', 'none'))

    $('#btn_limpiar').click(() => {
        añadir();
        enableButtons(CANCELAR);
        BloquearCampos(0);
    });
    $('#btn_añadir').click(() => {
        añadir();
    });
    $('#agregar_puc').click(() => {
        $("#puc").prop('disabled', false);
    });

    $('#btn_guardar').click(() => {
        Guardar();
    });

    $('#btn_edit').click(() => {
        enableButtons(EDITAR);
        BloquearCampos(1);
    })

    $('#btn_delete').click(() => {
        var codigo = $('#EmpIdStCodigo').val();
        confirmAlert("Sr. Usuario esta seguro que desea eliminar este registro?", "Atención", eliminar, codigo);
    });

    $("#btn_ubicacion").click(() => {
        CargarUbicacion();
        $("#tbl_ubicacion").css('display', 'block');
    });
    $('.close').click(() => $('#tbl_ubicacion').css('display', 'none'))
}