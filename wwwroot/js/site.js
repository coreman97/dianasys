﻿const INICIO = 0;
const NUEVO = 1;
const GUARDAR = 2;
const CANCELAR = 3;
const EDITAR = 4;
const ELIMINAR = 5;
const BUSCAR = 6;

$('#content').load("/Contabilidad/Empresas");
$('#btn_opc_sistema').click(() => {
    $('#content').load("/AdmSeguridad/OpcionesSistema");
});
$('#btn-gu').click(() => {
    $('#content').load("/AdmSeguridad/GrupoUsuarios");
});

$('#btn_mc').click(() => {
    $('#content').load("/Movimientos");
});

$('#btn_asociado').click(() => {
    $('#content').load("/Contabilidad/Asociados");
});

$('#btn_empresa').click(() => {
    $('#content').load("/Contabilidad/Empresas");
});

$('#btn_Tipodoc').click(() => {
    $('#content').load("/Contabilidad/TiposDeDocumentos");
});

$('#btn_TipoTer').click(() => {
    $('#content').load("/Contabilidad/TiposTerceros");
});

$('#btn_TipoCon').click(() => {
    $('#content').load("/Contabilidad/TiposComprobantes");
});

$('#btn_Centro').click(() => {
    $('#content').load("/Contabilidad/CentroCostos", { ASOCIADO, Empresa });
});
$('#btn_Subcentro').click(() => {
    $('#content').load("/Contabilidad/SubCentroDeCostos", { ASOCIADO, Empresa });
});

$('#btn_Comprobante').click(() => {
    $('#content').load("/Contabilidad/Comprobantes", { ASOCIADO, Empresa });
});

//----LOCALIZACION - GEOGRAFICA--------------------------------------------
$('#btn_Pais').click(() => {
    $('#content').load("/Localizacion/Pais");
});

$('#btn_Dpto').click(() => {
    $('#content').load("/Localizacion/Dpto");
});

$('#btn_Municipio').click(() => {
    $('#content').load("/Localizacion/Municipios");
});
//----------------------------------------------------------------------------

$('#btn_Terceros').click(() => {
    $('#content').load("/Contabilidad/Clientes", { ASOCIADO, Empresa });
});

$('#btn_Cartilla').click(() => {
    $('#content').load("/Cartilla/Cartilla", { ASOCIADO, Empresa });
});

$('#btn_Periodos').click(() => {
    $('#content').load("/Contabilidad/Periodos", { ASOCIADO, Empresa });
});

// ----------------- INFORMES --------------------------------------
$("#btn-libro-diario").click(() => {
    $('#content').load("/Informes/LibroDiario");
});

$("#btn_balance_comprobacion").click(() => {
    $('#content').load("/Informes/BalanceComprobacion");
});

$("#btn_estado_resultados").click(() => {
    $('#content').load("/Informes/EstadoResultados");
});


// -----------------------------------------------------------------

// Mostrar y ocultar opciones de Adm. Seguridad
var adm_show = false;
$('#adm_seg').click(() => {
    adm_show = !adm_show;
    if (adm_show) {
        $('#adm_show').removeClass("hidden")
    }
    else {
        $('#adm_show').addClass("hidden")
    }
});

// Mostrar y ocultar opciones de Contabilidad
var conta_show = false;
$('#contabilidad').click(() => {
    conta_show = !conta_show;
    if (conta_show) $('#conta_show').removeClass("hidden")
    else $('#conta_show').addClass("hidden")
});

var informe_show = false;
$("#informes").click(() => {
    informe_show = !informe_show;
    if (informe_show) $("#informe_show").removeClass("hidden")
    else $("#informe_show").addClass("hidden")
})

// Mostrar y ocultar la barra de la izquierda
var toggle_left = true;
$('#toggle-left').click(() => {
    console.log("Toggle")
    toggle_left = !toggle_left;
    if (toggle_left) {
        $('#left_nav').removeClass("hidden");
    }
    else $('#left_nav').addClass("hidden");
});

// se modifico la funcion de alerta
function confirmAlert(mensaje, titulo, okAccion, param, param1 = "", param2 = "", param3 = "") {
    var div = $('<div>');
    div.html(mensaje);
    div.attr('title', titulo);
    div.dialog({
        autoOpen: true,
        modal: true,
        dragglable: false,
        resizable: false,
        buttons: {
            "Ok": function () {
                okAccion(param, param1, param2, param3);
                $(this).dialog("close");
                div.remove();
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    })
}

function customAlert(mensaje, titulo = "Atención") {
    var div = $('<div>');
    div.html(mensaje);
    div.attr('title', titulo);
    div.dialog({
        autoOpen: true,
        modal: true,
        dragglable: false,
        resizable: false,
        buttons: {
            "Ok": function () {
                $(this).dialog("close");
                div.remove();
            }
        }
    })
}

function enableButtons(button) {
    /*
     * 1: nuevo, 2: guardar, 3: cancelar, 4: editar, 5: eliminar, 6: buscar
     */
    switch (button) {
        case 0: // inicial
            $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_guardar').attr('disabled', true).addClass('bg-blue-300');
            $('#btn_limpiar').attr('disabled', true).addClass('bg-blue-300');
            $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
            $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
            $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
            break;
        case 1: //nuevo
            $('#btn_guardar').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_limpiar').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_delete').attr('disabled', true).removeClass('bg-red-600').addClass('bg-red-300');
            $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_añadir').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            break;
        case 2: // guardar.
            $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_buscar').attr('disabled', true).addClass('bg-blue-600');
            $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_edit').attr('disabled', true).addClass('bg-blue-300');
            $('#btn_delete').attr('disabled', true).addClass('bg-red-300');
        case 3: // Deshacer
            $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
            $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
            $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
            break;
        case 4: // editar
            $('#btn_guardar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
            $('#btn_limpiar').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
            $('#btn_buscar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_edit').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
            $('#btn_añadir').attr('disabled', false).removeClass('bg-blue-600').addClass('bg-blue-300');
            break;
        case 5: // Eliminar
            $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_delete').attr('disabled', true).addClass('bg-red-300').removeClass('bg-red-600');
            $('#btn_edit').attr('disabled', true).addClass('bg-blue-300').removeClass('bg-blue-600');
            $('#btn_buscar').attr('disabled', false).addClass('bg-blue-600');
            $('#btn_añadir').attr('disabled', false).addClass('bg-blue-600');
            break;
        case 6: // buscar
            $('#btn_edit').attr('disabled', false).removeClass('bg-blue-300').addClass('bg-blue-600');
            $('#btn_delete').attr('disabled', false).removeClass('bg-red-300').addClass('bg-red-600');
            $('#btn_guardar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            $('#btn_limpiar').attr('disabled', true).removeClass('bg-blue-600').addClass('bg-blue-300');
            break;
    }
}