﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DianaSys.Controllers
{
    public class ContabilidadController : Controller
    {
        private readonly bddianasis_onlineContext _context;
        public ContabilidadController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region EMPRESAS
        public IActionResult Empresas()
        {
            return View();
        }

        [HttpGet("Contabilidad/empresas/list/{asociado}")]
        public ActionResult<IEnumerable<Empresas>> GetEmpresasList(int asociado)
        {
            return _context.Empresas.Where(e => e.EmpIdInAsociado == asociado).ToList();
        }

        [HttpGet("Contabilidad/empresas/{empresaId}/{asociado}")]
        public ActionResult<Empresas> GetEmpresaById(string empresaId, int asociado)
        {
            return _context.Empresas.FirstOrDefault(e => e.EmpIdInAsociado == asociado && e.EmpIdStCodigo == empresaId);
        }

        [HttpDelete("Contabilidad/empresas/{empresaId}/{asociado}")]
        public IActionResult DeleteEmpresa(string empresaId,int asociado)
        {
            try
            {
                _context.Remove(_context.Empresas.FirstOrDefault(e => e.EmpIdInAsociado == asociado && e.EmpIdStCodigo == empresaId));
                _context.SaveChanges();
                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/empresas/consecutivo/{asociado}")]
        public ActionResult<String> GetConsecutivoEmpresa(int asociado)
        {

            string Resultado = "1";
            try
            {
                if (_context.Empresas.Where(e => e.EmpIdInAsociado == asociado).Any())
                {
                    var maxAge = _context.Empresas.Where(e => e.EmpIdInAsociado == asociado).Max(e => e.EmpIdStCodigo);
                    Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
                }
                else
                {
                    Resultado = "1";
                }
            }
            catch
            {
                Resultado = "1";
            }
            return Resultado;
            
        }

        [HttpPost("Contabilidad/empresas/guardar")]
        public IActionResult PostEmpresa([FromBody] Empresas empresa)
        {
            if (empresa == null) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Empresas.Any(e => e.EmpIdInAsociado == empresa.EmpIdInAsociado && e.EmpIdStCodigo == empresa.EmpIdStCodigo))
            {
                _context.Entry(_context.Empresas.FirstOrDefault(e => e.EmpIdInAsociado == empresa.EmpIdInAsociado &&  e.EmpIdStCodigo == empresa.EmpIdStCodigo)).CurrentValues.SetValues(empresa);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Empresas.Add(empresa);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }

                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/empresas/tipospuc")]
        public ActionResult<IEnumerable<Contipospuc>> GetTiposPuc()
        {
            return _context.Contipospuc.ToList();
        }

        [HttpGet("Contabilidad/empresas/cartilla/{asociadoId}/{empresaId}/{usuario}/{puc}")]
        public ActionResult<bool> GenerarCartilla(int asociadoId, string empresaId, string usuario, string puc)
        {
            var cuentasPorPUC = _context.Conplanunicocuenta.Where(e => e.PucIdStTipoPuc == puc).ToList();
            var cuentasACopiar = new List<Concartilla>();
            foreach (var cuenta in cuentasPorPUC)
            {
                cuentasACopiar.Add(new Concartilla
                {
                    CarIdInAsociado = asociadoId,
                    CarIdStEmpresa = empresaId,
                    CatIdStUsuario = usuario,
                    CarIdStCuenta = cuenta.PucIdStCuenta,
                    CarIdStSubcuenta = cuenta.PucStSubcuenta,
                    CarIdStAuxiliar = cuenta.PucStAuxiliar,
                    CarIdStAuxiliarUno = cuenta.PucStAuxiliarUno,
                    CarStNombre = cuenta.PucStNombre,
                    CarStMovimiento = cuenta.PucStMovimiento,
                    CarStCentroCosto = cuenta.PucStCentroCosto,
                    CarStManejaNit = cuenta.PucSmanejatNit,
                    CarStNaturaleza = cuenta.PucStNaturaleza,
                    CarStManejaBase = cuenta.PucStManejaBase,
                    CarInPorcentajeRetencion = cuenta.PucInPorcentajeRetencion,
                    CarStAjustable = cuenta.PucStAjustable,
                    CarStIntegrado = cuenta.PucStIntegrado,
                    CarStVigencia = cuenta.PucStVigencia,
                    CarStUtilidad = cuenta.PucStUtilidad,
                });
            }

            try
            {
                _context.Concartilla.AddRange(cuentasACopiar);
                var result = _context.SaveChanges();

                if (result > 0) return true;
                return false;
            } catch (Exception e)
            {
                return false;
            }
        }

        [HttpGet("Contabilidad/empresas/VerificarCartilla/{empresaId}/{asociado}")]
        public ActionResult<bool> VerificarCartilla(string empresaId, int asociado)
        {
            return _context.Concartilla.Any(e => e.CarIdInAsociado == asociado && e.CarIdStEmpresa == empresaId);
        }
        #endregion

        #region ASOCIADOS
        /********************************
         *  CONTROLADORES DE ASOCIADOS  *
         ********************************/
        public IActionResult Asociados()
        {
            return View();
        }

        [HttpGet("Contabilidad/TipoDocumento")]
        public ActionResult<IEnumerable<Tipodocumentos>> GetTipoDocumentos()
        {
            return _context.Tipodocumentos.ToList();
        }

        [HttpGet("Contabilidad/municipios")]
        public ActionResult<IEnumerable<Municipio>> GetMunicipios()
        {
            return _context.Municipio.ToList();
        }

        [HttpGet("Contabilidad/municipio/{municipio}")]
        public ActionResult<Municipio> GetMunicipio(int municipio)
        {
            return _context.Municipio.First(e => e.MunIdInCodigo == municipio);
        }

        [HttpGet("Contabilidad/asociados/consecutivo")]
        public ActionResult<int> GetConsecutivo()
        {
            var ultimoRegistro = _context.Asociados.LastOrDefault();
            var consecutivo = 1;
            if (ultimoRegistro != null)
            {
                consecutivo = ultimoRegistro.AflIdInCodigo + 1;
            }
            return consecutivo;
        }

        [HttpGet("Contabilidad/asociados/list")]
        public ActionResult GetAsociadosList()
        {
            var result = _context.Asociados
             .Join(_context.Tipodocumentos, p => new { p1 = p.AflIdStTipoDocumento}, e => new { p1 = e.TdoIdStTipoDocumento },
                    (p, e) => new {
                        p.AflIdInCodigo,
                        p.AflIdStIdentificacion,
                        p.AflStNombreAfiliado,
                        p.AflIdStTipoDocumento,
                        e.TdoStDescripcion
                    }
                    ).ToList();

            return Ok(result);

           // return _context.Asociados.ToList();
        }

        [HttpGet("Contabilidad/asociados/{asociadoId}")]
        public ActionResult<Asociados> GetAsociadoById(int asociadoId)
        {
            return _context.Asociados.FirstOrDefault(e => e.AflIdInCodigo == asociadoId);
        }

        [HttpDelete("Contabilidad/asociados/{asociadoId}")]
        public IActionResult DeleteAsociado(int asociadoId)
        {
            _context.Remove(_context.Asociados.FirstOrDefault(e => e.AflIdInCodigo == asociadoId));
            _context.SaveChanges();
            return Ok();
        }

        [HttpPost("Contabilidad/asociados/guardar")]
        public IActionResult PostAsociado([FromBody] Asociados asociado)
        {
            if (asociado == null) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Asociados.Any(e => e.AflIdInCodigo == asociado.AflIdInCodigo))
            {
                _context.Entry(_context.Asociados.FirstOrDefault(e => e.AflIdInCodigo == asociado.AflIdInCodigo)).CurrentValues.SetValues(asociado);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Asociados.Add(asociado);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }

                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/VerificarAsociados/{asociadoId}/{Cedula}/{tipo}")]
        public ActionResult<bool> VericarAsociado(int asociadoId, string Cedula, bool tipo)
        {
            if (tipo)
            {
                return _context.Asociados.Any(e => e.AflIdStIdentificacion == Cedula);
            }
            else
            {
                return _context.Asociados.Any(e => e.AflIdStIdentificacion == Cedula && e.AflIdInCodigo != asociadoId );
            }
            
        }
        #endregion

        #region MOVIMIENTOS CONTABLES

        public IActionResult MovimientosContables()
        {
            return View();
        }

        [HttpGet("Contabilidad/movimientos/comprobantes")]
        public ActionResult<IEnumerable<Concomprobantes>> GetComprobantes()
        {
            return _context.Concomprobantes.ToList();
        }

        [HttpGet("Contabilidad/movimientos/terceros")]
        public ActionResult<IEnumerable<Clientes>> GetTerceros()
        {
            return _context.Clientes.ToList();
        }

        [HttpGet("Contabilidad/movimientos/ccosto")]
        public ActionResult<IEnumerable<Consubcentrocostos>> GetCentroCostos()
        {
            return _context.Consubcentrocostos.ToList();
        }

        [HttpGet("Contabilidad/movimientos/cuentas/{asociado}/{empresa}/{usuario}")]
        public ActionResult<IEnumerable<Concartilla>> GetCuentas(int asociado, string empresa, string usuario)
        {
            return _context.Concartilla.Where(e => e.CarIdInAsociado == asociado && e.CarIdStEmpresa == empresa
                && e.CatIdStUsuario == usuario && e.CarStMovimiento == "S").ToList();
        }

        [HttpGet("Contabilidad/movimientos/periodos")]
        public ActionResult<IEnumerable<Periodos>> GetPeriodos()
        {
            return _context.Periodos.ToList();
        }

        [HttpGet("Contabilidad/movimientos/periodo/{empresaid}/{asociado}")]
        public ActionResult<Empresas> GetPeriodo(string empresaid, int asociado)
        {
            return _context.Empresas.FirstOrDefault(e => e.EmpIdInAsociado == asociado && e.EmpIdStCodigo == empresaid);
        }

        [HttpGet("Contabilidad/movimientos/consecutivo/{empresa}/{asociado}/{usuario}")]
        public ActionResult<int> GetConsecutivoMovimiento(string empresa, int asociado, string usuario)
        {
            var actual = _context.Contmovimiento
                .LastOrDefault(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado
                && e.TmoIdStUsuario == usuario);
            if (actual == null) return 1;
            else
            {
                return actual.TmoIdInConsecutivo + 1;
            }
        }

        [HttpGet("Contabilidad/movimientos/list/{empresa}/{asociado}/{usuario}/{anio}/{mes}")]
        public ActionResult<IEnumerable<Contmovimiento>> GetMovimientos(int anio, int mes, string empresa, int asociado, 
            string usuario)
        {
            return _context.Contmovimiento.Where(e => 
                e.TmoIdInAno == anio && e.TmoIdInMes == mes && e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario &&
                e.TmoIdInAsociado == asociado)
                .ToList();
        }
        
        [HttpGet("Contabilidad/movimientos/encabezado/{empresa}/{asociado}/{usuario}/{consecutivo}")]
        public ActionResult<Contmovimiento> GetMovimientos(string empresa, int asociado, 
            string usuario, int consecutivo)
        {
            return _context.Contmovimiento.FirstOrDefault(e =>
                e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario &&
                e.TmoIdInAsociado == asociado && e.TmoIdInConsecutivo == consecutivo);
        }

        [HttpGet("Contabilidad/movimientos/detalles/{empresa}/{asociado}/{consecutivo}")]
        public ActionResult<IEnumerable<Conmovimientos>> GetDetallesMovimiento(string empresa, int asociado, int consecutivo)
        {
            return _context.Conmovimientos
                .Where(e => e.MovIdInAsociado == asociado && e.MovIdStEmpresa == empresa && e.MovIdInConsecutivo == consecutivo)
                .ToList();
        }

        [HttpPost("Contabilidad/movimientos/guardardetalle")]
        public IActionResult PostMovimientoDetalle([FromBody] Conmovimientos conmovimientos)
        {
            try
            {
                _context.Conmovimientos.Add(conmovimientos);
                var result = _context.SaveChanges();
                if (result > 0) return Ok();
                else return BadRequest();
            }
            catch
            {                
                return BadRequest();
            }
        }
        
        [HttpPost("Contabilidad/movimientos/guardarencabezado/{empresa}/{asociado}/{usuario}")]
        public IActionResult PostMovimientoEncabezado([FromBody] Contmovimiento contmovimiento, string empresa, int asociado, string usuario)
        {
            try
            {
                // Valida si el encabezado ya existe.
                var ultimoMovimiento = _context.Contmovimiento.LastOrDefault(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado && e.TmoIdStUsuario == usuario);
                if (ultimoMovimiento != null && ultimoMovimiento.TmoIdInConsecutivo == contmovimiento.TmoIdInConsecutivo)
                {
                    // Retorno OK ya que indica que si existe y que se esta tratando de insertar un detalle.
                    return Ok();
                }
                
                _context.Contmovimiento.Add(contmovimiento);
                var result = _context.SaveChanges();

                if (result > 0)
                {
                    var ultimo = _context.Contmovimiento.LastOrDefault(e => e.TmoIdInAsociado == asociado && e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario);
                    return Ok(ultimo);
                }

                return BadRequest();
            } catch
            {                
                return BadRequest();
            }
        }
        #endregion

        #region TipoDocuemntos - metodos de Tipos de documentos

        public void BuscarConsecutivo()
        {
            var maxAge = _context.Tipodocumentos.Max(e => e.TdoIdStTipoDocumento);
            ViewBag.NumMaximo = (Convert.ToInt32(maxAge) + 1).ToString();
        }
        public IActionResult TiposDeDocumentos()
        {
            var tipodoc = _context.Tipodocumentos.ToList();
            BuscarConsecutivo();
            return View(tipodoc);
        }
        [HttpPost("Contabilidad/Tipodoc/guardar")]
        public IActionResult PostTiposDocumentos([FromBody] Tipodocumentos Tipodocumentos)
        {
            if (Tipodocumentos.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Tipodocumentos.Any(e => e.TdoIdStTipoDocumento == Tipodocumentos.TdoIdStTipoDocumento))
            {
                _context.Entry(_context.Tipodocumentos.FirstOrDefault(e => e.TdoIdStTipoDocumento == Tipodocumentos.TdoIdStTipoDocumento)).CurrentValues.SetValues(Tipodocumentos);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Tipodocumentos.Add(Tipodocumentos);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                //ObtenerDatosSeleccionables();
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/Tipodocumento/{codigo}")]
        public ActionResult<Tipodocumentos> GetOpcionById(string codigo)
        {
            return _context.Tipodocumentos.First(e => e.TdoIdStTipoDocumento == codigo);
        }

        [HttpPut("Contabilidad/Tipodocumento/actualizar")]
        public IActionResult ActualizarTipoDocumento([FromBody] Tipodocumentos Tipodocumentos)
        {
            if (Tipodocumentos.Equals(null)) return BadRequest("Datos invalidos.");
            _context.Entry(_context.Tipodocumentos.FirstOrDefault(e => e.TdoIdStTipoDocumento == Tipodocumentos.TdoIdStTipoDocumento)).CurrentValues.SetValues(Tipodocumentos);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("Contabilidad/Tipodocumento/delete/{codigo}")]
        public IActionResult EliminarOpcion(string codigo)
        {
            try
            {
                _context.Remove(_context.Tipodocumentos.FirstOrDefault(e => e.TdoIdStTipoDocumento == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/Tipodocumento/refrescar")]
        public ActionResult<IEnumerable<Tipodocumentos>> RefreshDatosSeleccionables()
        {

            var Tipodoc = _context.Tipodocumentos.Select(x => new {
                x.TdoIdStTipoDocumento,
                x.TdoStCodigoDian,
                x.TdoStDescripcion,
                x.TdoStRequerido,
                x.TdoStUsuario,
                x.TdoDtFechaHora

            }).ToList();

            var maxAge = Tipodoc.Max(e => e.TdoIdStTipoDocumento);
            ViewBag.NumMaximo = (Convert.ToInt32(maxAge) + 1).ToString();
            return Ok(Tipodoc);
        }

        [HttpGet("Contabilidad/Tipodocumento/Consecutivo")]
        public ActionResult<string> BusquedaConsecutivo()
        {
            var maxAge = _context.Tipodocumentos.Max(e => e.TdoIdStTipoDocumento);
            ViewBag.NumMaximo = (Convert.ToInt32(maxAge) + 1).ToString();
            string Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
            return Resultado;

        }
        [HttpGet("Contabilidad/Tipodocumento/HallarConsecutivo")]
        public ActionResult Resultado()
        {
            var maxAge = _context.Tipodocumentos.Max(e => e.TdoIdStTipoDocumento);
            ViewBag.NumMaximo = (Convert.ToInt32(maxAge) + 1).ToString();
            return View();

        }
        #endregion

        #region TipoTerceros - metodos de Tipos de documentos

        public IActionResult TiposTerceros()
        {
            var tipotercero = _context.Tiposterceros.ToList();
            return View(tipotercero);
        }

        [HttpPost("Contabilidad/Tipoterceros/guardar")]
        public IActionResult PostTiposTerceros([FromBody] Tiposterceros Tiposterceros)
        {
            if (Tiposterceros.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Tiposterceros.Any(e => e.TteIdStCodigo == Tiposterceros.TteIdStCodigo))
            {
                _context.Entry(_context.Tiposterceros.FirstOrDefault(e => e.TteIdStCodigo == Tiposterceros.TteIdStCodigo)).CurrentValues.SetValues(Tiposterceros);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Tiposterceros.Add(Tiposterceros);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/Tipoterceros/{codigo}")]
        public ActionResult<Tiposterceros> BuscarTipoTercero(string codigo)
        {
            return _context.Tiposterceros.First(e => e.TteIdStCodigo == codigo);
        }
        [HttpPut("Contabilidad/Tipoterceros/actualizar")]
        public IActionResult ActualizarTipoTerceros([FromBody] Tiposterceros Tiposterceros)
        {
            if (Tiposterceros.Equals(null)) return BadRequest("Datos invalidos.");
            _context.Entry(_context.Tiposterceros.FirstOrDefault(e => e.TteIdStCodigo == Tiposterceros.TteIdStCodigo)).CurrentValues.SetValues(Tiposterceros);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("Contabilidad/Tipoterceros/delete/{codigo}")]
        public IActionResult EliminarTipoTErcero(string codigo)
        {
            try
            {
                _context.Remove(_context.Tiposterceros.FirstOrDefault(e => e.TteIdStCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/Tipoterceros/refrescar")]
        public ActionResult<IEnumerable<Tiposterceros>> RefreshTiposTerceros()
        {

            var TipoTer = _context.Tiposterceros.Select(x => new {
                x.TteIdStCodigo,
                x.TteStNombre,
                x.TteDtFechaHora,
                x.TteStClase,
                x.TteStUsuario

            }).ToList();
            return Ok(TipoTer);
        }

        [HttpGet("Contabilidad/Tipoterceros/Consecutivo")]
        public ActionResult<string> BusConsecutivoTipoTercero()
        {
            var maxAge = _context.Tiposterceros.Max(e => e.TteIdStCodigo);
            string Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
            return Resultado;

        }

        #endregion

        #region TipoComprobantes - metodos de Tipos de comprobantes

        public IActionResult TiposComprobantes()
        {
            var tipocomprobante = _context.Contiposcomprobantes.ToList();
            return View(tipocomprobante);
        }

        [HttpPost("Contabilidad/TiposComprobantes/guardar")]
        public IActionResult PostTiposComprobantes([FromBody] Contiposcomprobantes Contiposcomprobantes)
        {
            if (Contiposcomprobantes.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Contiposcomprobantes.Any(e => e.TcpIdStTipoComprobante == Contiposcomprobantes.TcpIdStTipoComprobante))
            {
                _context.Entry(_context.Contiposcomprobantes.FirstOrDefault(e => e.TcpIdStTipoComprobante == Contiposcomprobantes.TcpIdStTipoComprobante)).CurrentValues.SetValues(Contiposcomprobantes);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Contiposcomprobantes.Add(Contiposcomprobantes);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/TiposComprobantes/{codigo}")]
        public ActionResult<Contiposcomprobantes> BuscarTipoComprobantes(string codigo)
        {
            try
            {
                return _context.Contiposcomprobantes.First(e => e.TcpIdStTipoComprobante == codigo);
            }
            catch
            {
                return null;
            }
        }
        [HttpPut("Contabilidad/TiposComprobantes/actualizar")]
        public IActionResult ActualizarTipoComprobantes([FromBody] Contiposcomprobantes Contiposcomprobantes)
        {
            if (Contiposcomprobantes.Equals(null)) return BadRequest("Datos invalidos.");
            _context.Entry(_context.Contiposcomprobantes.FirstOrDefault(e => e.TcpIdStTipoComprobante == Contiposcomprobantes.TcpIdStTipoComprobante)).CurrentValues.SetValues(Contiposcomprobantes);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("Contabilidad/TiposComprobantes/delete/{codigo}")]
        public IActionResult EliminarTipoComprobante(string codigo)
        {
            try
            {
                _context.Remove(_context.Contiposcomprobantes.FirstOrDefault(e => e.TcpIdStTipoComprobante == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/TiposComprobantes/refrescar")]
        public ActionResult<IEnumerable<Contiposcomprobantes>> RefreshTiposComprobantes()
        {

            var TipoComprobante = _context.Contiposcomprobantes.Select(x => new {
                x.TcpIdStTipoComprobante,
                x.TcpStDescripcion,
                x.TcpDtFechaHora,
                x.TcpIdStUsuario

            }).ToList();
            return Ok(TipoComprobante);
        }

        [HttpGet("Contabilidad/TiposComprobantes/Consecutivo")]
        public ActionResult<string> BusConsecutivoTipoTComprobante()
        {
            var maxAge = _context.Contiposcomprobantes.Max(e => e.TcpIdStTipoComprobante);
            string Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
            return Resultado;

        }

        [HttpGet("Contabilidad/TiposComprobantes/ValidarCodigo/{codigo}")]
        public ActionResult<bool> BuscarTipoTComprobante(string codigo)
        {
            return _context.Contiposcomprobantes.Any(e => e.TcpIdStTipoComprobante == codigo);

        }

        #endregion

        #region Centros de costos - metodos de Centros de costos

        public IActionResult CentroCostos(int Asociado, string empresa)
        {
            var centrocostos = _context.Concentrocostos.Where(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa).ToList();
            return View(centrocostos);
        }

        [HttpPost("Contabilidad/CentroCosto/guardar")]
        public IActionResult PostCentroCostos([FromBody] Concentrocostos Concentrocostos)
        {
            if (Concentrocostos.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Concentrocostos.Any(e => e.CenIdStCodigo == Concentrocostos.CenIdStCodigo
                && e.CenIdInAsociado == Concentrocostos.CenIdInAsociado
                && e.CenIdStEmpresa == Concentrocostos.CenIdStEmpresa))
            {
                _context.Entry(_context.Concentrocostos.FirstOrDefault(e => e.CenIdInAsociado == Concentrocostos.CenIdInAsociado && e.CenIdStEmpresa == Concentrocostos.CenIdStEmpresa && e.CenIdStCodigo == Concentrocostos.CenIdStCodigo)).CurrentValues.SetValues(Concentrocostos);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Concentrocostos.Add(Concentrocostos);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/CentroCosto/{codigo}/{Asociado}/{empresa}")]
        public ActionResult<Concentrocostos> BuscarCentroCostos(string codigo, int Asociado, string empresa)
        {
            try
            {
                return _context.Concentrocostos.First(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa && e.CenIdStCodigo == codigo);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Contabilidad/CentroCosto/delete/{codigo}/{Asociado}/{empresa}")]
        public IActionResult EliminarCentroCostos(string codigo, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Concentrocostos.FirstOrDefault(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa && e.CenIdStCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/CentroCosto/refrescar/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Concentrocostos>> RefreshCentroCostos(int Asociado, string empresa)
        {

            var CentroCosto = _context.Concentrocostos.Where(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa).Select(x => new {
                x.CenIdStCodigo,
                x.CenStNombre,
                x.CenIdInAsociado,
                x.CenIdStEmpresa

            }).ToList();
            return Ok(CentroCosto);
        }


        [HttpGet("Contabilidad/CentroCosto/Consecutivo/{Asociado}/{empresa}")]
        public ActionResult<string> BusConsecutivoCentroCostos(int Asociado, string empresa)
        {
            //&& e.CenIdStEmpresa == empresa
            var maxAge = _context.Concentrocostos.Where(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa).Max(e => e.CenIdStCodigo);
            string Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
            return Resultado;

        }

        [HttpGet("Contabilidad/CentroCosto/ValidarCodigo/{codigo}/{Asociado}/{empresa}")]
        public ActionResult<bool> BuscarCentroCosto(string codigo, int Asociado, string empresa)
        {
            return _context.Concentrocostos.Any(e => e.CenIdStCodigo == codigo);

        }

        #endregion

        #region SubCentros de costos - metodos de SubCentros de costos

        public void ObtenerCentroCostos(int Asociado, string empresa)
        {
            var centrosc = _context.Concentrocostos.Where(e => e.CenIdInAsociado == Asociado && e.CenIdStEmpresa == empresa).ToList();
            ViewBag.centrosc = centrosc;

        }
        public IActionResult SubCentroDeCostos(int Asociado, string empresa)
        {
            var subcentrocostos = _context.Consubcentrocostos.Where(e => e.SceIdInAsociado == Asociado && e.SceIdStEmpresa == empresa).ToList();
            ObtenerCentroCostos(Asociado, empresa);
            return View(subcentrocostos);
        }

        [HttpPost("Contabilidad/SubCentroCosto/guardar")]
        public IActionResult PostSubCentroCostos([FromBody] Consubcentrocostos Consubcentrocostos)
        {
            if (Consubcentrocostos.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Consubcentrocostos.Any(e => e.SceIdStCodigo == Consubcentrocostos.SceIdStCodigo
                && e.SceIdInAsociado == Consubcentrocostos.SceIdInAsociado
                && e.SceIdStEmpresa == Consubcentrocostos.SceIdStEmpresa
                && e.SceIdStCentro == Consubcentrocostos.SceIdStCentro))
            {
                _context.Entry(_context.Consubcentrocostos.FirstOrDefault(e => e.SceIdInAsociado == Consubcentrocostos.SceIdInAsociado && e.SceIdStEmpresa == Consubcentrocostos.SceIdStEmpresa && e.SceIdStCentro == Consubcentrocostos.SceIdStCentro && e.SceIdStCodigo == Consubcentrocostos.SceIdStCodigo)).CurrentValues.SetValues(Consubcentrocostos);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Consubcentrocostos.Add(Consubcentrocostos);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/SubCentroCosto/{codigo}/{centro}/{Asociado}/{empresa}")]
        public ActionResult<Consubcentrocostos> BuscarSubCentroCostos(string codigo, string centro, int Asociado, string empresa)
        {
            try
            {
                return _context.Consubcentrocostos.First(e => e.SceIdInAsociado == Asociado && e.SceIdStEmpresa == empresa && e.SceIdStCentro == centro && e.SceIdStCodigo == codigo);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Contabilidad/SubCentroCosto/delete/{codigo}/{centro}/{Asociado}/{empresa}")]
        public IActionResult EliminarSubCentroCostos(string codigo, string centro, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Consubcentrocostos.FirstOrDefault(e => e.SceIdInAsociado == Asociado && e.SceIdStEmpresa == empresa && e.SceIdStCentro == centro && e.SceIdStCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/SubCentroCosto/refrescar/{Asociado}/{empresa}")]
        public ActionResult RefreshSubCentroCostos(int Asociado, string empresa)
        {

            var result = (from s in _context.Consubcentrocostos
                          join e in _context.Concentrocostos on new { s1 = s.SceIdInAsociado, s2 = s.SceIdStEmpresa, s3 = s.SceIdStCentro } equals new { s1 = e.CenIdInAsociado, s2 = e.CenIdStEmpresa, s3 = e.CenIdStCodigo}
                          select new
                          {
                              s.SceIdInAsociado,
                              s.SceIdStEmpresa,
                              s.SceIdStCentro,
                              s.SceIdStCodigo,
                              s.SceStNombre,
                              s.SceStEstado,
                              e.CenStNombre
                          }).ToList();


            return Ok(result);
            
        }



        [HttpGet("Contabilidad/SubCentroCosto/Consecutivo/{Centro}/{Asociado}/{empresa}")]
        public ActionResult<string> BusConsecutivoSubCentroCostos(string Centro, int Asociado, string empresa)
        {
            var maxAge = _context.Consubcentrocostos.Where(e => e.SceIdInAsociado == Asociado && e.SceIdStEmpresa == empresa && e.SceIdStCentro == Centro).Max(e => e.SceIdStCodigo);
            string Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
            return Resultado;

        }

        [HttpGet("Contabilidad/SubCentroCosto/ValidarCodigo/{codigo}/{Asociado}/{empresa}")]
        public ActionResult<bool> BuscarSubCentroCosto(string codigo, int Asociado, string empresa)
        {
            return _context.Concentrocostos.Any(e => e.CenIdStCodigo == codigo);

        }

        #endregion

        #region Comprobantes - metodos de Comprobantes

        [HttpGet("Contabilidad/Cuentacontable/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Concartilla>> GetCartilla(int Asociado, string empresa)
        {
            return _context.Concartilla.Where(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa).ToList();
        }

        public void ObtenerTipoComprobante()
        {
            var tiposcom = _context.Contiposcomprobantes.ToList();
            ViewBag.tipocomprobante = tiposcom;
        }
        public IActionResult Comprobantes(int Asociado, string empresa)
        {
            ObtenerTipoComprobante();
            var comprobante = _context.Concomprobantes.Where(e => e.ComIdInAsociado == Asociado && e.ComIdStEmpresa == empresa).ToList();
            return View(comprobante);
        }

        [HttpPost("Contabilidad/Comprobantes/guardar")]
        public IActionResult PostComprobantes([FromBody] Concomprobantes Concomprobantes)
        {
            if (Concomprobantes.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Concomprobantes.Any(e => e.ComIdStCodigo == Concomprobantes.ComIdStCodigo
                && e.ComIdInAsociado == Concomprobantes.ComIdInAsociado
                && e.ComIdStEmpresa == Concomprobantes.ComIdStEmpresa))
            {
                Concomprobantes.ComDtFechaHora = DateTime.Now;

                _context.Entry(_context.Concomprobantes.FirstOrDefault(e => e.ComIdInAsociado == Concomprobantes.ComIdInAsociado && e.ComIdStEmpresa == Concomprobantes.ComIdStEmpresa && e.ComIdStCodigo == Concomprobantes.ComIdStCodigo)).CurrentValues.SetValues(Concomprobantes);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Concomprobantes.Add(Concomprobantes);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception EX)
            {
                throw EX;
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/Comprobantes/{codigo}/{Asociado}/{empresa}")]
        public ActionResult<Concomprobantes> BuscarComprobante(string codigo, int Asociado, string empresa)
        {
            try
            {
                return _context.Concomprobantes.First(e => e.ComIdInAsociado == Asociado && e.ComIdStEmpresa == empresa && e.ComIdStCodigo == codigo);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Contabilidad/Comprobantes/delete/{codigo}/{Asociado}/{empresa}")]
        public IActionResult EliminarComprobante(string codigo, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Concomprobantes.FirstOrDefault(e => e.ComIdInAsociado == Asociado && e.ComIdStEmpresa == empresa && e.ComIdStCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/Comprobantes/refrescar/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Concomprobantes>> RefreshComprobante(int Asociado, string empresa)
        {

            var Comprobante = _context.Concomprobantes.Where(e => e.ComIdInAsociado == Asociado && e.ComIdStEmpresa == empresa).Select(x => new {
                x.ComIdStCodigo,
                x.ComStNombre,
                x.ComInNumeroInicial,
                x.ComInNumeroFinal,
                x.ComInNumero,
                x.ComStAutomatico,
                x.ComIdStTipoComprobante

            }).ToList();
            return Ok(Comprobante);
        }


        [HttpGet("Contabilidad/Comprobantes/ValidarCodigo/{codigo}/{Asociado}/{empresa}")]
        public ActionResult<bool> BuscarComprobantes(string codigo, int Asociado, string empresa)
        {
            return _context.Concomprobantes.Any(e => e.ComIdInAsociado == Asociado && e.ComIdStEmpresa == empresa && e.ComIdStCodigo == codigo);

        }

        #endregion

        #region Terceros o Clientes  - metodos de clientes

        [HttpGet("Contabilidad/CargarTipodocumento")]
        public ActionResult<IEnumerable<Tipodocumentos>> GetTipodocumento()
        {
            return _context.Tipodocumentos.ToList();
        }

        public void ObtenerTiposDeDocumentos()
        {
            var tiposdoc = _context.Tipodocumentos.ToList();
            ViewBag.tipodocumento = tiposdoc;
        }
        public IActionResult Clientes(int Asociado, string empresa)
        {
            ObtenerTiposDeDocumentos();
            var cliente = _context.Clientes.Where(e => e.CliIdInAsociado == Asociado && e.CliIdStEmpresa == empresa).ToList();
            return View(cliente);
        }

        [HttpPost("Contabilidad/Clientes/guardar")]
        public IActionResult PostClientes([FromBody] Clientes Clientes)
        {
            if (Clientes.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Clientes.Any(e => e.CliIdStNit == Clientes.CliIdStNit
                && e.CliIdInAsociado == Clientes.CliIdInAsociado
                && e.CliIdStEmpresa == Clientes.CliIdStEmpresa))
            {
                Clientes.CliDtFechaHora = DateTime.Now;

                _context.Entry(_context.Clientes.FirstOrDefault(e => e.CliIdInAsociado == Clientes.CliIdInAsociado && e.CliIdStEmpresa == Clientes.CliIdStEmpresa && e.CliIdStNit == Clientes.CliIdStNit)).CurrentValues.SetValues(Clientes);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Clientes.Add(Clientes);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception EX)
            {
                return BadRequest();
            }
        }

        [HttpPost("Contabilidad/Clientes/guardarDetalle")]
        public IActionResult GuardarDetallesTercero([FromBody] List<RTipoclientes> rTipoclientes)
        {
            try
            {
                var tipos = _context.RTipoclientes.Where(e => e.RtiIdStAsociado == rTipoclientes[0].RtiIdStAsociado
                    && e.RtiIdStEmpresa == rTipoclientes[0].RtiIdStEmpresa && e.RtiIdStNitCliente == rTipoclientes[0].RtiIdStNitCliente).ToList();
                _context.RTipoclientes.RemoveRange(tipos);
                _context.SaveChanges();
                _context.RTipoclientes.AddRange(rTipoclientes);
                var result = _context.SaveChanges();
                if (result > 0) return Ok();
                else return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("Contabiliadad/Clientes/detalles/{nit}/{asociado}/{empresa}")]
        public ActionResult<IEnumerable<RTipoclientes>> ObtenerDetalles(string nit, int asociado, string empresa)
        {
            return _context.RTipoclientes.Where(e => e.RtiIdStEmpresa == empresa
                && e.RtiIdStAsociado == asociado && e.RtiIdStNitCliente == nit).ToList();
        }

        [HttpGet("Contabilidad/Clientes/{nit}/{Asociado}/{empresa}")]
        public ActionResult<Clientes> BuscarClientes(string nit, int Asociado, string empresa)
        {
            try
            {
                return _context.Clientes.First(e => e.CliIdInAsociado == Asociado && e.CliIdStEmpresa == empresa && e.CliIdStNit == nit);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Contabilidad/Clientes/delete/{nit}/{Asociado}/{empresa}")]
        public IActionResult EliminarClientes(string nit, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Clientes.FirstOrDefault(e => e.CliIdInAsociado == Asociado && e.CliIdStEmpresa == empresa && e.CliIdStNit == nit));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/Clientes/refrescar/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Clientes>> RefreshClientes(int Asociado, string empresa)
        {

            var cliente = _context.Clientes.Where(e => e.CliIdInAsociado == Asociado && e.CliIdStEmpresa == empresa).Select(x => new
            {
                x.CliIdInAsociado,
                x.CliIdStEmpresa,
                x.CliIdStipoDocumento,
                x.CliIdStNit,
                x.CliStDigito,
                x.CliStNombreCliente,
                x.CliIdStUsuario,
                x.CliInCupoCredito,
                x.CliInDiasGracia,
                x.CliStDireccion,
                x.CliInDptoLocalizacion,
                x.CliInMuniLocalizacion,
                x.CliInPaisLocalizacion,
                x.ClirStResidencia,
                x.CliStApartadoAereo,
                x.CliStAutoretenedor,
                x.CliStCelular,
                x.CliStCiudad,
                x.CliStCliRetecionRenta,
                x.CliStCliRetencionIca,
                x.CliStCliRetencionIva,
                x.CliStCorreoElectronico,
                x.CliStDeclarante,
                x.CliStGranContribuyente,
                x.CliStProRetencionIca,
                x.CliStProRetencionIva,
                x.CliStProRetencionRenta,
                x.CliStRegimenTributario,
                x.CliStRenta,
                x.CliStResponsableIva,
                x.CliStRetefuente,
                x.CliStPrimerApellido,
                x.CliStPrimerNombre,
                x.CliStSegundoApellido,
                x.CliStSegundoNombre,
                x.CliStSexo,
                x.CliStTelefono,
                x.CliStVendedorAsignado,
                x.CliStVigencia


            }).ToList();
            return Ok(cliente);
        }


        [HttpGet("Contabilidad/Clientes/ValidarNit/{nit}/{Asociado}/{empresa}")]
        public ActionResult<bool> BuscarCliente(string nit, int Asociado, string empresa)
        {
            return _context.Clientes.Any(e => e.CliIdInAsociado == Asociado && e.CliIdStEmpresa == empresa && e.CliIdStNit == nit);

        }

        [HttpGet("Contabilidad/Clientes/TiposTerceros")]
        public ActionResult<IEnumerable<Clientes>> CargarTiposTerceros()
        {

            var tterceros = _context.Tiposterceros.Select(x => new
            {
                x.TteIdStCodigo,
                x.TteStNombre

            }).ToList();
            return Ok(tterceros);
        }

        #endregion

        #region Periodos Contable  - metodos de Periodos contables

        public IActionResult Periodos(int Asociado, string empresa)
        {
            var periodo = _context.Periodos.Where(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa).ToList();
            return View();
        }

        [HttpPost("Contabilidad/PeriodosContable/guardar")]
        public IActionResult PostPeriodosContable([FromBody] Periodos Periodos)
        {
            if (Periodos.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Periodos.Any(e => e.PerIdInAsociado == Periodos.PerIdInAsociado
                && e.PerIdStEmpresa == Periodos.PerIdStEmpresa
                && e.PerInAno == Periodos.PerInAno
                && e.PerInMes == Periodos.PerInMes
                && e.PerStAplicacion == Periodos.PerStAplicacion))
            {
                Periodos.PerDtFechaHora = DateTime.Now;

                _context.Entry(_context.Periodos.FirstOrDefault(e => e.PerIdInAsociado == Periodos.PerIdInAsociado && e.PerIdStEmpresa == Periodos.PerIdStEmpresa && e.PerInAno == Periodos.PerInAno && e.PerInMes == Periodos.PerInMes && e.PerStAplicacion == Periodos.PerStAplicacion)).CurrentValues.SetValues(Periodos);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Periodos.Add(Periodos);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception EX)
            {                
                return BadRequest();
            }
        }

        [HttpGet("Contabilidad/PeriodosContable/{ano}/{mes}/{aplicacion}/{Asociado}/{empresa}")]
        public ActionResult<Periodos> BuscarPeriodosContable(int ano, int mes, string aplicacion, int Asociado, string empresa)
        {
            try
            {
                return _context.Periodos.First(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa && e.PerInAno == ano && e.PerInMes == mes && e.PerStAplicacion == aplicacion);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Contabilidad/PeriodosContable/delete/{ano}/{mes}/{aplicacion}/{Asociado}/{empresa}")]
        public IActionResult EliminarPeriodosContable(int ano, int mes, string aplicacion, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Periodos.FirstOrDefault(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa && e.PerInAno == ano && e.PerInMes == mes && e.PerStAplicacion == aplicacion));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Contabilidad/PeriodosContable/refrescar/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Periodos>> RefreshPeriodosContable(int Asociado, string empresa)
        {

            var periodo = _context.Periodos.Where(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa).Select(x => new
            {
                x.PerIdInAsociado,
                x.PerIdStEmpresa,
                x.PerInAno,
                x.PerInMes,
                x.PerStAplicacion,
                x.PerStEstado

            }).ToList();
            return Ok(periodo);
        }


        [HttpGet("Contabilidad/PeriodosContable/ValidarPeriodo/{ano}/{mes}/{aplicacion}/{Asociado}/{empresa}")]
        public ActionResult<bool> VerificarPeriodosContable(int ano, int mes, string aplicacion, int Asociado, string empresa)
        {
            return _context.Periodos.Any(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa && e.PerInAno == ano && e.PerInMes == mes && e.PerStAplicacion == aplicacion);

        }

        [HttpGet("Contabilidad/PeriodosContable/Consecutivo/{ano}/{aplicacion}/{Asociado}/{empresa}")]
        public ActionResult<string> BusConsecutivoPeriodo(int ano, string aplicacion, int Asociado, string empresa)
        {
            string Resultado = "0";
            try
            {
                if (_context.Periodos.Where(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa && e.PerInAno == ano && e.PerStAplicacion == aplicacion).Any())
                {

                    var maxAge = _context.Periodos.Where(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa && e.PerInAno == ano && e.PerStAplicacion == aplicacion).Max(e => e.PerInMes);
                    Resultado = (Convert.ToInt32(maxAge) + 1).ToString();
                }
                else
                {
                    Resultado = "1";
                }
            }
            catch
            {
                Resultado = "1";
            }
            return Resultado;

        }


        #endregion

        #region Generales

        [HttpGet("Contabilidad/Municipios/Listado")]
        public ActionResult ListadoMunicipios()
        {

            var result = (from ep in _context.Municipio
                          join e in _context.Dpto on new { ep1 = ep.MunIdInPais, ep2 = ep.MunIdInDpto } equals new { ep1 = e.DptIdInPais, ep2 = e.DptIdInCodigo }
                          join t in _context.Pais on ep.MunIdInPais equals t.PaiIdInCodigo
                          select new
                          {
                              ep.MunIdInCodigo,
                              ep.MunIdInDpto,
                              ep.MunIdInPais,
                              ep.MunStNombre,
                              e.DptStNombre,
                              t.PaiStNombre
                          }).ToList();


            return Ok(result);

        }

        [HttpGet("Contabilidad/Municipios/Buscar/{codigo}/{pais}/{dpto}")]
        public ActionResult<Municipio> BuscarMunicipios(int codigo, int pais, int dpto)
        {
            try
            {
                return _context.Municipio.First(e => e.MunIdInPais == pais && e.MunIdInDpto == dpto && e.MunIdInCodigo == codigo);

            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}