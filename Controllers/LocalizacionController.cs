﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Data;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DianaSys.Controllers
{
    public class LocalizacionController : Controller
    {
        private readonly bddianasis_onlineContext _context;

        public LocalizacionController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region Dpto - metodos de Dpto

        public void ObtenerPaises()
        {
            var paises = _context.Pais.ToList();
            ViewBag.paises = paises;
        }

        public IActionResult Dpto()
        {
            ObtenerPaises();
            var dpto = _context.Dpto.ToList();
            return View(dpto);
        }

        [HttpPost("Dpto/guardar")]
        public IActionResult PostDepartamentos([FromBody] Dpto Dpto)
        {
            if (Dpto.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Dpto.Any(e => e.DptIdInPais == Dpto.DptIdInPais && e.DptIdInCodigo == Dpto.DptIdInCodigo))
            {
                _context.Entry(_context.Dpto.FirstOrDefault(e => e.DptIdInPais == Dpto.DptIdInPais && e.DptIdInCodigo == Dpto.DptIdInCodigo)).CurrentValues.SetValues(Dpto);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Dpto.Add(Dpto);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Dpto/{codigo}/{pais}")]
        public ActionResult<Dpto> BuscarDepartamento(int codigo, int pais)
        {
            try
            {
                return _context.Dpto.First(e => e.DptIdInPais == pais && e.DptIdInCodigo == codigo);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Dpto/delete/{codigo}/{pais}")]
        public IActionResult EliminarDepartamento(int codigo, int pais)
        {
            try
            {
                _context.Remove(_context.Dpto.FirstOrDefault(e => e.DptIdInPais == pais && e.DptIdInCodigo== codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Dpto/refrescar")]
        public ActionResult RefreshDepartamento()
        {
            var result = _context.Dpto
             .Join(_context.Pais, p => new { p1 = p.DptIdInPais }, e => new { p1 = e.PaiIdInCodigo },
                    (p, e) => new {
                        p.DptIdInPais,
                        p.DptIdInCodigo,
                        p.DptStNombre,
                        p.DptUsuario,
                        e.PaiStNombre
                    }
                    ).ToList();

            return Ok(result);
                        
        }

        [HttpGet("Dpto/ValidarCodigo/{codigo}/{pais}")]
        public ActionResult<bool> Buscarpaises(int codigo, int pais)
        {
            return _context.Dpto.Any(e => e.DptIdInPais == pais && e.DptIdInCodigo== codigo);

        }

        #endregion

        #region Municipio - metodos de Municipio
        
        [HttpGet("DptoPaises")]
        public ActionResult<IEnumerable<Pais>> GetDptoPaises()
        {
            return _context.Pais.ToList();
        }

        [HttpGet("Dptomunicipios/{pais}")]
        public ActionResult<IEnumerable<Dpto>> GetDptoMunicipios(int pais)
        {
            return _context.Dpto.Where(e =>e.DptIdInPais == pais).ToList();
            
        }

        [HttpGet("BuscarDpto/{pais}/{dpto}")]
        public ActionResult<Municipio> GetMunicipio(int municipio)
        {
            return _context.Municipio.First(e => e.MunIdInCodigo == municipio);
        }
                
        public IActionResult Municipios()
        {
            ObtenerPaises();
            var municipio = _context.Municipio.ToList();
            return View(municipio);
        }

        [HttpPost("Municipios/guardar")]
        public IActionResult PostMunicipios([FromBody] Municipio Municipio)
        {
            if (Municipio.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Municipio.Any(e => e.MunIdInPais == Municipio.MunIdInPais && e.MunIdInDpto == Municipio.MunIdInDpto && e.MunIdInCodigo == Municipio.MunIdInCodigo))
            {
                _context.Entry(_context.Municipio.FirstOrDefault(e => e.MunIdInPais == Municipio.MunIdInPais && e.MunIdInDpto == Municipio.MunIdInDpto && e.MunIdInCodigo == Municipio.MunIdInCodigo)).CurrentValues.SetValues(Municipio);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Municipio.Add(Municipio);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Municipios/{codigo}/{pais}/{dpto}")]
        public ActionResult<Municipio> BuscarMunicipios(int codigo, int pais, int dpto)
        {
            try
            {
                return _context.Municipio.First(e => e.MunIdInPais == pais && e.MunIdInDpto == dpto && e.MunIdInCodigo == codigo);
                
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Municipios/delete/{codigo}/{pais}/{dpto}")]
        public IActionResult EliminarMunicipios(int codigo, int pais, int dpto)
        {
            try
            {
                _context.Remove(_context.Municipio.FirstOrDefault(e => e.MunIdInPais == pais  && e.MunIdInDpto== dpto && e.MunIdInCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Municipios/refrescar")]
        public ActionResult RefreshMunicipios()
        {

            var result = (from ep in _context.Municipio
                         join e in _context.Dpto on new { ep1 = ep.MunIdInPais, ep2 = ep.MunIdInDpto } equals new {ep1 = e.DptIdInPais, ep2 = e.DptIdInCodigo}                                       
                         join t in _context.Pais on ep.MunIdInPais equals t.PaiIdInCodigo
                         select new
                         {
                            ep.MunIdInCodigo,
                            ep.MunIdInDpto,
                            ep.MunIdInPais,
                            ep.MunStNombre,
                            e.DptStNombre,
                            t.PaiStNombre
                            }).ToList();


            return Ok(result);
            
        }

        [HttpGet("Municipios/ValidarCodigo/{codigo}/{pais}/{dpto}")]
        public ActionResult<bool> BuscarMunicipio(int codigo, int pais, int dpto)
        {
            return _context.Municipio.Any(e => e.MunIdInPais == pais && e.MunIdInDpto == dpto && e.MunIdInCodigo == codigo);

        }

        #endregion

        #region Pais - metodos de pais

        public IActionResult Pais()
        {
            var pais = _context.Pais.ToList();
            return View(pais);
        }

        [HttpPost("Pais/guardar")]
        public IActionResult Postpaises([FromBody] Pais Pais)
        {
            if (Pais.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Pais.Any(e => e.PaiIdInCodigo == Pais.PaiIdInCodigo))
            {
                _context.Entry(_context.Pais.FirstOrDefault(e => e.PaiIdInCodigo == Pais.PaiIdInCodigo)).CurrentValues.SetValues(Pais);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Pais.Add(Pais);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("Pais/{codigo}")]
        public ActionResult<Pais> BuscarPaises(int codigo)
        {
            try
            {
                return _context.Pais.First(e => e.PaiIdInCodigo == codigo);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("Pais/delete/{codigo}")]
        public IActionResult EliminarPaises(int codigo)
        {
            try
            {
                _context.Remove(_context.Pais.FirstOrDefault(e => e.PaiIdInCodigo == codigo));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("Pais/refrescar")]
        public ActionResult<IEnumerable<Pais>> RefreshPaises()
        {

            var paises = _context.Pais.Select(x => new {
                x.PaiIdInCodigo,
                x.PaiStNombre,
                x.PaiStCodigoDian,
                x.PaiStUsuario

            }).ToList();
            return Ok(paises);
        }

        [HttpGet("Pais/ValidarCodigo/{codigo}")]
        public ActionResult<bool> Buscarpaises(int codigo)
        {
            return _context.Pais.Any(e => e.PaiIdInCodigo == codigo);

        }

        #endregion

        #region - consultas de informes
        [HttpGet("Informes/LibroDiario/{asociado}/{empresa}/{fechai}/{Fechaf}/{comprobante}/{numero}")]
        public ActionResult InformeLibroDiario(int asociado, string empresa, DateTime fechai, DateTime Fechaf, int comprobante, string numero)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo} equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          join t in _context.Concomprobantes on new { ep6 = ep.TmoIdInAsociado, ep7 = ep.TmoIdStEmpresa, ep8 = ep.TmoIdStComprobante } equals new { ep6 = t.ComIdInAsociado, ep7 = t.ComIdStEmpresa, ep8 = t.ComIdStCodigo}
                          join c in _context.Clientes on new {e1 = e.MovIdInAsociado, e2 = e.MovIdStEmpresa, e3 = e.MovIdStNit } equals new { e1 = c.CliIdInAsociado, e2 =c.CliIdStEmpresa, e3 = c.CliIdStNit} into lj
                          from x in lj.DefaultIfEmpty()
                          join s in _context.Consubcentrocostos on new { e4 = e.MovIdInAsociado, e5 = e.MovIdStEmpresa, e6 = e.MovIdStCentro,e7 = e.MovIdStSubcentro } equals new { e4 = s.SceIdInAsociado, e5 = s.SceIdStEmpresa, e6 = s.SceIdStCentro,e7= s.SceIdStCodigo } into lm
                          from m in lm.DefaultIfEmpty()
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoDtFecha >= fechai && ep.TmoDtFecha<= Fechaf 
                          orderby ep.TmoIdStComprobante,ep.TmoIdInConsecutivo
                          select new
                          {
                              ep.TmoIdStComprobante,
                              ep.TmoStDescripcion,
                              ep.TmoStNumero,
                              ep.TmoIdInConsecutivo,
                              ep.TmoDtFecha,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCuenta,
                              e.MovIdStNit,
                              e.MovIdStSubcentro,
                              e.MovIdStCentro,
                              e.MovIdStSubcuenta,
                              e.MovInItem,
                              e.MovInIvaMayorValor,
                              e.MovInValor,
                              e.MovStDescripcion,
                              x.CliStNombreCliente,
                              m.SceStNombre,
                              t.ComStNombre
                          }).ToList();


            return Ok(result);

        }
        #endregion
    }
}