﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DianaSys.Controllers
{
    public class MovimientosController : Controller
    {
        private readonly bddianasis_onlineContext _context;

        public MovimientosController(bddianasis_onlineContext ctx)
        {
            this._context = ctx;
        }
        public IActionResult Index()
        {
            return View();
        }

        #region MOVIMIENTOS CONTABLES

        public IActionResult MovimientosContables()
        {
            return View();
        }

        [HttpGet("periodos/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Periodos>> Periodos(int Asociado, string empresa)
        {
            var periodo = _context.Periodos.Where(e => e.PerIdInAsociado == Asociado && e.PerIdStEmpresa == empresa).ToList();
            return periodo;
        }

        [HttpGet("comprobantes")]
        public ActionResult<IEnumerable<Concomprobantes>> GetComprobantes()
        {
            return _context.Concomprobantes.ToList();
        }

        [HttpGet("comprobante/{asociado}/{empresa}/{usuario}/{comprobante}")]
        public ActionResult<Concomprobantes> GetComprobanteById(int asociado, string empresa, string usuario,
            string comprobante)
        {
            return _context.Concomprobantes.FirstOrDefault(e => e.ComIdInAsociado == asociado
            && e.ComIdStEmpresa == empresa && e.ComIdStCodigo == comprobante);
        }

        [HttpGet("csnumero/{asociado}/{empresa}/{usuario}/{comprobante}")]
        public ActionResult<int> GetNumeroConsecutivo(int asociado, string empresa,
            string comprobante)
        {
            if (_context.Concomprobantes.Any(e => e.ComIdStEmpresa == empresa && e.ComIdInAsociado == asociado
                 && e.ComIdStCodigo == comprobante))
            {
                var actual = _context.Concomprobantes.Where(e => e.ComIdStEmpresa == empresa && e.ComIdInAsociado == asociado
                    && e.ComIdStCodigo == comprobante).Max(e => e.ComInNumero);

                return actual + 1;
            }
            else
            {
                return 1;
            }
        }

        [HttpGet("terceros")]
        public ActionResult<IEnumerable<Clientes>> GetTerceros()
        {
            return _context.Clientes.ToList();
        }

        [HttpGet("ccosto")]
        public ActionResult<IEnumerable<Consubcentrocostos>> GetCentroCostos()
        {
            return _context.Consubcentrocostos.ToList();
        }

        [HttpGet("cuentas/{asociado}/{empresa}/{usuario}")]
        public ActionResult<IEnumerable<Concartilla>> GetCuentas(int asociado, string empresa, string usuario)
        {
            return _context.Concartilla.Where(e => e.CarIdInAsociado == asociado && e.CarIdStEmpresa == empresa
                && e.CatIdStUsuario == usuario && e.CarStMovimiento == "1").ToList();
        }

        [HttpGet("cuentas/{asociado}/{empresa}/{usuario}/{cuenta}/{subcuenta}/{auxiliar}/{auxiliar1}")]
        public ActionResult<Concartilla> GetConfigurarionCuenta(int asociado, string empresa, string usuario,
            string cuenta, string subcuenta, string auxiliar, string auxiliar1)
        {
            return _context.Concartilla.FirstOrDefault(e => e.CarIdInAsociado == asociado && e.CarIdStEmpresa == empresa
                && e.CatIdStUsuario == usuario && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta
                && e.CarIdStAuxiliar == auxiliar && e.CarIdStAuxiliarUno == auxiliar1);
        }

        [HttpGet("periodos")]
        public ActionResult<IEnumerable<Periodos>> GetPeriodos()
        {
            return _context.Periodos.ToList();
        }

        [HttpGet("periodo/{empresaid}/{asociado}")]
        public ActionResult<Empresas> GetPeriodo(string empresaid, int asociado)
        {
            return _context.Empresas.FirstOrDefault(e => e.EmpIdInAsociado == asociado && e.EmpIdStCodigo == empresaid);
        }

        [HttpGet("consecutivo/{empresa}/{asociado}/{usuario}")]
        public ActionResult<int> GetConsecutivoMovimiento(string empresa, int asociado, string usuario)
        {
            if (_context.Contmovimiento
                .Any(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado
                && e.TmoIdStUsuario == usuario))
            {
                var actual = _context.Contmovimiento
                .Where(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado
                && e.TmoIdStUsuario == usuario).Max(e => e.TmoIdInConsecutivo);

                return actual + 1;
            }
            else return 1;
        }

        [HttpGet("list/{empresa}/{asociado}/{usuario}/{anio}/{mes}")]
        public ActionResult<IEnumerable<Contmovimiento>> GetMovimientos(int anio, int mes, string empresa, int asociado,
            string usuario)
        {
            return _context.Contmovimiento.Where(e =>
                e.TmoIdInAno == anio && e.TmoIdInMes == mes && e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario &&
                e.TmoIdInAsociado == asociado)
                .ToList();
        }

        [HttpGet("encabezado/{empresa}/{asociado}/{usuario}/{consecutivo}")]
        public ActionResult<Contmovimiento> GetMovimientos(string empresa, int asociado,
            string usuario, int consecutivo)
        {
            return _context.Contmovimiento.FirstOrDefault(e =>
                e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario &&
                e.TmoIdInAsociado == asociado && e.TmoIdInConsecutivo == consecutivo);
        }

        [HttpGet("detalles/{empresa}/{asociado}/{consecutivo}")]
        public ActionResult<IEnumerable<Conmovimientos>> GetDetallesMovimiento(string empresa, int asociado, int consecutivo)
        {
            return _context.Conmovimientos
                .Where(e => e.MovIdInAsociado == asociado && e.MovIdStEmpresa == empresa && e.MovIdInConsecutivo == consecutivo)
                .ToList();
        }

        [HttpGet("detalle/{empresa}/{asociado}/{item}")]
        public ActionResult<Conmovimientos> GetMovimientoById(string empresa, int asociado, int item)
        {
            return _context.Conmovimientos.FirstOrDefault(e => e.MovIdStEmpresa == empresa && e.MovIdInAsociado == asociado
                && e.MovInItem == item);
        }

        [HttpPut("actualizarDetalle/{empresa}/{asociado}/{detalleId}")]
        public IActionResult PutMovimientoDetalle([FromBody] Conmovimientos detalle, string empresa, int asociado, int detalleId)
        {
            try
            {
                if (detalle.Equals(null)) return BadRequest();
                _context.Entry(_context.Conmovimientos.FirstOrDefault(e => e.MovIdStEmpresa == empresa
                    && e.MovIdInAsociado == asociado && e.MovInItem == detalleId)).CurrentValues.SetValues(detalle);
                _context.SaveChanges();

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPost("guardardetalle/{empresa}/{asociado}/{consecutivo}")]
        public IActionResult PostMovimientoDetalle([FromBody] IEnumerable<Conmovimientos> conmovimientos, string empresa, int asociado
            , int consecutivo)
        {
            try
            {
                if (_context.Contmovimiento.Any(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado &&
                    e.TmoIdInConsecutivo == consecutivo))
                {
                    foreach (var movimiento in conmovimientos)
                    {
                        _context.Conmovimientos.Add(movimiento);
                    }

                    var result = _context.SaveChanges();
                    if (result > 0) return Ok();
                    else return BadRequest();
                }
                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        [HttpPost("guardarencabezado/{empresa}/{asociado}/{usuario}")]
        public IActionResult PostMovimientoEncabezado([FromBody] Contmovimiento contmovimiento, string empresa, int asociado, string usuario)
        {
            try
            {
                // Valida si el encabezado ya existe.
                var ultimoMovimiento = _context.Contmovimiento.Any(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado
                    && e.TmoIdStUsuario == usuario && e.TmoIdInConsecutivo == contmovimiento.TmoIdInConsecutivo);
                if (ultimoMovimiento)
                {
                    // Retorno OK ya que indica que si existe y que se esta tratando de insertar un detalle.
                    return Ok();
                }

                _context.Contmovimiento.Add(contmovimiento);
                var actualizarNumeroComprobante = _context.Concomprobantes.FirstOrDefault(e => e.ComIdStEmpresa == empresa && e.ComIdInAsociado == asociado &&
                    e.ComIdStCodigo == contmovimiento.TmoIdStComprobante);
                actualizarNumeroComprobante.ComInNumero = int.Parse(contmovimiento.TmoStNumero);
                _context.Concomprobantes.Update(actualizarNumeroComprobante);
                var result = _context.SaveChanges();

                if (result > 0)
                {
                    var ultimo = _context.Contmovimiento.LastOrDefault(e => e.TmoIdInAsociado == asociado && e.TmoIdStEmpresa == empresa && e.TmoIdStUsuario == usuario);
                    return Ok(ultimo);
                }

                return BadRequest();
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // Valida si existe el consecutivo en los detalles antes de eliminar de la tabla.
        [HttpGet("validarExistencia/{empresa}/{asociado}/{consecutivo}")]
        public bool ValidarConsecutivoEnDetalles(string empresa, int asociado, int consecutivo)
        {
            return _context.Conmovimientos.Any(e => e.MovIdStEmpresa == empresa && e.MovIdInAsociado == asociado
                && e.MovIdInConsecutivo == consecutivo);
        }

        [HttpDelete("borrarDetalle/{empresa}/{asociado}/{usuario}/{item}")]
        public IActionResult DeleteDetalle(string empresa, int asociado, string usuario, int item)
        {
            var itemToDelete = _context.Conmovimientos.FirstOrDefault(e => e.MovIdStEmpresa == empresa &&
                e.MovIdInAsociado == asociado && e.MovInItem == item);
            _context.Conmovimientos.Remove(itemToDelete);
            int result = _context.SaveChanges();
            if (result > 0)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        [HttpDelete("borrarMovimiento/{empresa}/{asociado}/{usuario}/{consecutivo}")]
        public IActionResult DeleteMovimiento(string empresa, int asociado, string usuario, int consecutivo)
        {
            try
            {
                var detalles = _context.Conmovimientos.Where(e => e.MovIdStEmpresa == empresa &&
                    e.MovIdInAsociado == asociado && e.MovIdInConsecutivo == consecutivo).ToList();
                _context.Conmovimientos.RemoveRange(detalles);
                var detallesDeleted = _context.SaveChanges();

                var encabezado = _context.Contmovimiento.FirstOrDefault(e => e.TmoIdStEmpresa == empresa && e.TmoIdInAsociado == asociado
                        && e.TmoIdStUsuario == usuario && e.TmoIdInConsecutivo == consecutivo);

                _context.Contmovimiento.Remove(encabezado);
                var result = _context.SaveChanges();

                if (result > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch
            {
                return BadRequest();
            }
        }

        [HttpGet("diferencia/{empresa}/{asociado}/{comprobante}")]
        public ActionResult<decimal> GetDiferencia(string empresa, int asociado, string comprobante)
        {
            try
            {
                return _context.Conmovimientos.Where(e => e.MovIdStEmpresa == empresa && e.MovIdInAsociado == asociado
                    && e.MovIdStComprobante == comprobante).Sum(e => e.MovInValor);
            }
            catch
            {
                return BadRequest();
            }
        }
        #endregion
    }
}