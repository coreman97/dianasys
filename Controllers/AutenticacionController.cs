﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DianaSys.Entities;
using DianaSys.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DianaSys.Controllers
{
    public class AutenticacionController : Controller
    {
        private readonly bddianasis_onlineContext _context;
        public AutenticacionController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }

        private string MD5(string s)
        {
            using (var provider = System.Security.Cryptography.MD5.Create())
            {
                StringBuilder builder = new StringBuilder();
                foreach (var b in provider.ComputeHash(Encoding.UTF8.GetBytes(s)))
                {
                    builder.Append(b.ToString("x2").ToLower());
                }
                return builder.ToString();
            }
        }
        
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("autenticacion/empresas/{username}")]
        public ActionResult<IEnumerable<Empresas>> GetEmpresasPorAsociado(string username)
        {
            try
            {
                var user = _context.Segusuarios.FirstOrDefault(u => u.UsuStIdUsuario == username);
                var empresas = _context.Empresas.Where(e =>
                    e.EmpIdInAsociado == user.UsuIdInAsociado)
                    .Select(x => new { 
                        x.EmpStNombre,
                        x.EmpIdStCodigo
                    }).ToList();
                return Ok(empresas);
            } catch(Exception e)
            {
                return BadRequest();
            }
        }

        [HttpGet("autenticacion/getAsociado/{username}")]
        public ActionResult<int> GetAsociado(string username)
        {
            try
            {
                var user = _context.Segusuarios.FirstOrDefault(u => u.UsuStIdUsuario == username);                
                return Ok(user.UsuIdInAsociado);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }
        
        
        [HttpPost("Autenticacion/login")]
        public IActionResult Login([FromBody] UserAuth user)
        {
            var usuario = _context.Segusuarios.FirstOrDefault(u =>
                u.UsuStIdUsuario == user.username && u.UsuStClave == MD5(user.contraseña) 
                && u.UsuIdStEmpresa == user.empresa);
            if (usuario != null)
            {
                HttpContext.Session.SetString("Usuario", user.username);
                HttpContext.Session.SetString("Empresa", user.empresa);
                HttpContext.Session.SetInt32("Asociado", usuario.UsuIdInAsociado);
                return Redirect("/");
            }
            else
            {
                return BadRequest();
            }
        }
    }
}