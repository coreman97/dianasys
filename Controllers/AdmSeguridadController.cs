﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Data;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DianaSys.Controllers
{
    public class AdmSeguridadController : Controller
    {
        private readonly bddianasis_onlineContext _context;

        public AdmSeguridadController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }

        [HttpGet("AdmSeguridad/Empresas")]
        public ActionResult<IEnumerable<Empresas>> Empresas()
        {
            var empresas = new Test(_context).GetEmpresas();
            return Ok(empresas);
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult GrupoUsuarios()
        {
            return View();
        }

        public void ObtenerDatosSeleccionables()
        {
            var nivel = _context.Segnivelopcion.ToList();
            var padres = _context.Segopciones.Where(e => e.OpcStTipoOpcion == "P").ToList();
            ViewBag.padres = padres;
            ViewBag.nivel = nivel;
            ViewBag.nPadres = padres.Count;
        }

        public IActionResult OpcionesSistema()
        {
            ObtenerDatosSeleccionables();
            var opciones = _context.Segopciones.ToList();
            return View(opciones);
        }

        [HttpGet("AdmSeguridad/opciones/refrescar")]
        public ActionResult<IEnumerable<Segopciones>> RefreshDatosSeleccionables()
        {
            ObtenerDatosSeleccionables();
            var opciones = _context.Segopciones.Select(x => new {
                x.OpcIdInNivel,
                x.OpcIdStOpcion,
                x.OpcIdStPadre,
                x.OpcInOrden,
                x.OpcStAdicionar,
                x.OpcStConsultar,
                x.OpcStDescripcion,
                x.OpcStEliminar,
                x.OpcStFormulario,
                x.OpcStImprimir,
                x.OpcStModificar,
                x.OpcStTipoOpcion
            }).ToList();
            return Ok(opciones);
        }

        [HttpPost("AdmSeguridad/opciones/guardar")]
        public IActionResult PostOpcionesSistema([FromBody] Segopciones segopciones)
        {
            if (segopciones == null) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Segopciones.Any(e => e.OpcIdStOpcion == segopciones.OpcIdStOpcion))
            {
                _context.Entry(_context.Segopciones.FirstOrDefault(e => e.OpcIdStOpcion == segopciones.OpcIdStOpcion)).CurrentValues.SetValues(segopciones);
                _context.SaveChanges();
                return Ok();
            }

            try {
                _context.Segopciones.Add(segopciones);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ObtenerDatosSeleccionables();
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            } catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("AdmSeguridad/opciones/{codigo}")]
        public ActionResult<Segopciones> GetOpcionById(string codigo)
        {
            return _context.Segopciones.First(e => e.OpcIdStOpcion == codigo);
        }

        [HttpGet("AdmSeguridad/opciones/padre/{codigo}")]
        public ActionResult<Segopciones> GetPadre(string codigo)
        {
            return _context.Segopciones.First(e => e.OpcIdStOpcion == codigo);
        }

        [HttpGet("AdmSeguridad/opciones/exist/{codigo}")]
        public ActionResult<bool> DoCodigoExists(string codigo)
        {
            return _context.Segopciones.Any(e => e.OpcIdStOpcion == codigo);
        }

        [HttpPut("AdmSeguridad/opciones/actualizar")]
        public IActionResult ActualizarOpcion([FromBody] Segopciones segopciones)
        {
            if (segopciones.Equals(null)) return BadRequest("Datos invalidos.");
            _context.Entry(_context.Segopciones.FirstOrDefault(e => e.OpcIdStOpcion == segopciones.OpcIdStOpcion)).CurrentValues.SetValues(segopciones);
            _context.SaveChanges();
            return Ok();
        }

        [HttpDelete("AdmSeguridad/opciones/delete/{codigo}")]
        public IActionResult EliminarOpcion(string codigo)
        {
            _context.Remove(_context.Segopciones.FirstOrDefault(e => e.OpcIdStOpcion == codigo));
            _context.SaveChanges();
            return Ok();
        }
     }
}