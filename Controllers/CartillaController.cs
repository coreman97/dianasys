﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Data;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;


namespace DianaSys.Controllers
{
    public class CartillaController : Controller
    {
        private readonly bddianasis_onlineContext _context;

        public CartillaController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }
        public IActionResult Index()
        {
            return View();
        }


        public IActionResult Cartilla(int Asociado, string empresa)
        {
            var cartilla = _context.Concartilla.Where(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa).ToList();
            return View(cartilla);
        }

        [HttpPost("CartillaContable/guardar")]
        public IActionResult PostCartillaContabale([FromBody] Concartilla Concartilla)
        {
            if (Concartilla.Equals(null)) return BadRequest("Datos invalidos.");
            // Valida si el registro existe
            // Si existe lo actualiza, si no, lo crea.
            if (_context.Concartilla.Any(e => e.CarIdStCuenta == Concartilla.CarIdStCuenta
                && e.CarIdStSubcuenta == Concartilla.CarIdStSubcuenta
                && e.CarIdStAuxiliar == Concartilla.CarIdStAuxiliar
                && e.CarIdStAuxiliarUno == Concartilla.CarIdStAuxiliarUno
                && e.CarIdInAsociado == Concartilla.CarIdInAsociado
                && e.CarIdStEmpresa == Concartilla.CarIdStEmpresa))
            {
                Concartilla.CarDtFechaHora = DateTime.Now;

                _context.Entry(_context.Concartilla.FirstOrDefault(e => e.CarIdInAsociado == Concartilla.CarIdInAsociado && e.CarIdStEmpresa == Concartilla.CarIdStEmpresa && e.CarIdStCuenta == Concartilla.CarIdStCuenta && e.CarIdStSubcuenta == Concartilla.CarIdStSubcuenta && e.CarIdStAuxiliar == Concartilla.CarIdStAuxiliar && e.CarIdStAuxiliarUno == Concartilla.CarIdStAuxiliarUno)).CurrentValues.SetValues(Concartilla);
                _context.SaveChanges();
                return Ok();
            }

            try
            {
                _context.Concartilla.Add(Concartilla);
                var result = _context.SaveChanges();

                if (!(result > 0))
                {
                    return BadRequest();
                }
                ViewBag.mensaje = "Se ha guardado correctamente.";
                return Ok();
            }
            catch (Exception EX)
            {               
                return BadRequest();
            }
        }

        [HttpGet("CartillaContable/{cuenta}/{subcuenta}/{aux}/{aux1}/{Asociado}/{empresa}")]
        public ActionResult<Concartilla> BuscarCartillaContable(string cuenta, string subcuenta, string aux, string aux1, int Asociado, string empresa)
        {
            try
            {
                return _context.Concartilla.First(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarIdStAuxiliarUno == aux1);
            }
            catch
            {
                return null;
            }
        }

        [HttpDelete("CartillaContable/delete/{cuenta}/{subcuenta}/{aux}/{aux1}/{Asociado}/{empresa}")]
        public IActionResult EliminarCartillaContable(string cuenta, string subcuenta, string aux, string aux1, int Asociado, string empresa)
        {
            try
            {
                _context.Remove(_context.Concartilla.FirstOrDefault(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarIdStAuxiliarUno == aux1));
                _context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound();
            }

        }

        [HttpGet("CartillaContable/refrescar/{Asociado}/{empresa}")]
        public ActionResult<IEnumerable<Concartilla>> RefreshCartillaContable(int Asociado, string empresa)
        {

            var cartilla = _context.Concartilla.Where(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa).Select(x => new
            {
                x.CarIdInAsociado,
                x.CarIdStEmpresa,
                x.CarIdStCuenta,
                x.CarIdStSubcuenta,
                x.CarIdStAuxiliar,
                x.CarIdStAuxiliarUno,
                x.CarStNombre,
                x.CarDtFechaHora,
                x.CarInPorcentajeRetencion,
                x.CarStAjustable,
                x.CarStCentroCosto,
                x.CarStIntegrado,
                x.CarStManejaBase,
                x.CarStManejaNit,
                x.CarStMovimiento,
                x.CarStNaturaleza,
                x.CarStUtilidad,
                x.CarStVigencia,
                x.CatIdStUsuario

            }).ToList();
            return Ok(cartilla);
        }


        [HttpGet("CartillaContable/ValidarCuenta/{cuenta}/{subcuenta}/{aux}/{aux1}/{Asociado}/{empresa}")]
        public ActionResult<bool> VerificarCartillaContable(string cuenta, string subcuenta, string aux, string aux1, int Asociado, string empresa)
        {
            return _context.Concartilla.Any(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarIdStAuxiliarUno == aux1);

        }

        [HttpGet("CartillaContable/EstructuraGeneral/{cuenta}/{subcuenta}/{aux}/{aux1}/{Asociado}/{empresa}/{nivel}")]
        public ActionResult<Concartilla> ValidarEstructuraCartillaContable(string cuenta, string subcuenta, string aux, string aux1, int Asociado, string empresa, int nivel)
        {
            try
            {
                var Datos = _context.Concartilla.First(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta);
                switch (nivel)
                {
                    case 2:
                        Datos = _context.Concartilla.First(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta);
                        break;
                    case 3:
                        Datos = _context.Concartilla.First(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux);
                        break;
                    case 4:
                        Datos = _context.Concartilla.First(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarIdStAuxiliarUno == aux1);
                        break;
                }
                return Datos;
            }
            catch
            {
                return null;
            }
        }

        [HttpGet("CartillaContable/Estructura/{cuenta}/{subcuenta}/{aux}/{aux1}/{Asociado}/{empresa}/{nivel}")]
        public ActionResult<bool> ValidarEstructuraCartilla(string cuenta, string subcuenta, string aux, string aux1, int Asociado, string empresa, int nivel)
        {
            try
            {
                var Datos = _context.Concartilla.Any(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarStMovimiento == "1");
                switch (nivel)
                {
                    case 2:
                        Datos = _context.Concartilla.Any(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarStMovimiento == "1");
                        break;
                    case 3:
                        Datos = _context.Concartilla.Any(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarStMovimiento == "1");
                        break;
                    case 4:
                        Datos = _context.Concartilla.Any(e => e.CarIdInAsociado == Asociado && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta == subcuenta && e.CarIdStAuxiliar == aux && e.CarIdStAuxiliarUno == aux1 && e.CarStMovimiento == "1");
                        break;
                }
                return Datos;
            }
            catch
            {
                return false;
            }
        }
    }
}