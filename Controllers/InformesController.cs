﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DianaSys.Entities;
using Microsoft.AspNetCore.Mvc;

namespace DianaSys.Controllers
{
    public class InformesController : Controller
    {
        private readonly bddianasis_onlineContext _context;
        public InformesController(bddianasis_onlineContext ctx)
        {
            _context = ctx;
        }

        public IActionResult LibroDiario()
        {
            return View();
        }

        public IActionResult BalanceComprobacion()
        {
            return View();
        }

        public IActionResult EstadoResultados()
        {
            return View();
        }

        [HttpGet("Informes/Padres/{asociado}/{empresa}/{cuenta}/{sub}")]
        public ActionResult CuentaPadres(int asociado, string empresa, string cuenta, string sub)
        {
            var padres = _context.Concartilla.Where(e => e.CarIdInAsociado == asociado
                && e.CarIdStEmpresa == empresa && e.CarIdStCuenta == cuenta && e.CarIdStSubcuenta != sub && int.Parse(e.CarIdStSubcuenta) <= int.Parse(sub)).ToList();
            return Ok(padres);
        }

        //[HttpGet("Informes/LibroDiario/{asociado}/{empresa}/{fechai}/{Fechaf}/{comprobante}/{numero}")]
        [HttpGet("Informes/LibroDiario/{asociado}/{empresa}/{fechai}/{Fechaf}")]
        public ActionResult InformeLibroDiario(int asociado, string empresa, string fechai, string fechaf, int comprobante, string numero)
        {
            DateTime fechaInicial = DateTime.Parse(fechai);
            DateTime fechaFinal = DateTime.Parse(fechaf);
            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          join t in _context.Concomprobantes on new { ep6 = ep.TmoIdInAsociado, ep7 = ep.TmoIdStEmpresa, ep8 = ep.TmoIdStComprobante } equals new { ep6 = t.ComIdInAsociado, ep7 = t.ComIdStEmpresa, ep8 = t.ComIdStCodigo }
                          join c in _context.Clientes on new { e1 = e.MovIdInAsociado, e2 = e.MovIdStEmpresa, e3 = e.MovIdStNit } equals new { e1 = c.CliIdInAsociado, e2 = c.CliIdStEmpresa, e3 = c.CliIdStNit } into lj
                          from x in lj.DefaultIfEmpty()
                          join s in _context.Consubcentrocostos on new { e4 = e.MovIdInAsociado, e5 = e.MovIdStEmpresa, e6 = e.MovIdStCentro, e7 = e.MovIdStSubcentro } equals new { e4 = s.SceIdInAsociado, e5 = s.SceIdStEmpresa, e6 = s.SceIdStCentro, e7 = s.SceIdStCodigo } into lm
                          from m in lm.DefaultIfEmpty()
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoDtFecha >= fechaInicial && ep.TmoDtFecha <= fechaFinal
                          orderby ep.TmoIdStComprobante, ep.TmoIdInConsecutivo
                          select new
                          {
                              ep.TmoIdStComprobante,
                              ep.TmoStDescripcion,
                              ep.TmoStNumero,
                              ep.TmoIdInConsecutivo,
                              ep.TmoDtFecha,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCuenta,
                              e.MovIdStNit,
                              e.MovIdStSubcentro,
                              e.MovIdStCentro,
                              e.MovIdStSubcuenta,
                              e.MovInItem,
                              e.MovInIvaMayorValor,
                              e.MovInValor,
                              e.MovStDescripcion,
                              x.CliStNombreCliente,
                              m.SceStNombre,
                              t.ComStNombre
                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceComprobacion/Movimientos/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeBalanceComprobacion(int asociado, string empresa, int ano, int mes)
        {
            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          join c in _context.Concartilla on new { c1 = e.MovIdStCuenta, c2 = e.MovIdStSubcuenta, c3 = e.MovIdStAuxiliar, c4 = e.MovIdStAuxiliarUno } equals new { c1 = c.CarIdStCuenta, c2 = c.CarIdStSubcuenta, c3 = c.CarIdStAuxiliar, c4 = c.CarIdStAuxiliarUno }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes < mes && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                              e.MovIdStNit,
                              c.CarStNombre
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              MovInValor = x.Sum(s => s.MovInValor),
                              Nit = x.Key.MovIdStNit,
                              CuentaNombre = x.Key.CarStNombre
                          }).ToList();
            return Ok(result);
        }

        [HttpGet("Informes/BalanceComprobacion/Debitos/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeBalanceComprobacionDebitos(int asociado, string empresa, int ano, int mes)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes == mes && e.MovInValor > 0 && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              MovInValor = x.Sum(s => s.MovInValor)
                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceComprobacion/Creditos/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeBalanceComprobacionCreditos(int asociado, string empresa, int ano, int mes)
        {
            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes == mes && e.MovInValor < 0 && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              MovInValor = x.Sum(s => s.MovInValor)

                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceComprobacion/Saldos/{asociado}/{empresa}/{ano}")]
        public ActionResult InformeBalanceComprobacionSaldos(int asociado, string empresa, int ano)
        {
            int ano1 = ano - 1;
            var result = (from ep in _context.Consaldos
                          where ep.SalIdInAsociado == asociado && ep.SalIdStEmpresa == empresa && ep.SalIdInAno == ano1 && ep.SalIdInMes == 12 && ep.SalIdsStAplicacion == "0"
                          group ep by new
                          {
                              ep.SalIdStCuenta,
                              ep.SalIdStSubcuenta,
                              ep.SalIdStAuxiliar,
                              ep.SalIdStAuxiliarUno,
                              ep.SalIdStCentro,
                              ep.SalIdStSubcentro,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.SalIdStCuenta,
                              Subcuenta = x.Key.SalIdStSubcuenta,
                              Auxiliar = x.Key.SalIdStAuxiliar,
                              AuxiliarUno = x.Key.SalIdStAuxiliarUno,
                              Centro = x.Key.SalIdStCentro,
                              Subcentro = x.Key.SalIdStSubcentro,
                              MovInValor = x.Sum(s => s.SalInValor)

                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceGeneral/Movimientos/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeBalanceGeneral(int asociado, string empresa, int ano, int mes)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes <= mes && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                              e.MovIdStNit,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              Nit = x.Key.MovIdStNit,
                              MovInValor = x.Sum(s => s.MovInValor)

                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceGeneral/MovimientosMes/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeBalanceGeneralMes(int asociado, string empresa, int ano, int mes)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes == mes && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                              e.MovIdStNit,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              Nit = x.Key.MovIdStNit,
                              MovInValor = x.Sum(s => s.MovInValor)

                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/BalanceGeneral/Saldos/{asociado}/{empresa}/{ano}")]
        public ActionResult InformeBalanceGeneralSaldos(int asociado, string empresa, int ano)
        {
            int ano1 = ano - 1;
            var result = (from ep in _context.Consaldos
                          where ep.SalIdInAsociado == asociado && ep.SalIdStEmpresa == empresa && ep.SalIdInAno == ano && ep.SalIdInMes == 12 && ep.SalIdsStAplicacion == "0"
                          group ep by new
                          {
                              ep.SalIdStCuenta,
                              ep.SalIdStSubcuenta,
                              ep.SalIdStAuxiliar,
                              ep.SalIdStAuxiliarUno,
                              ep.SalIdStCentro,
                              ep.SalIdStSubcentro,
                              ep.SalIdStNit,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.SalIdStCuenta,
                              Subcuenta = x.Key.SalIdStSubcuenta,
                              Auxiliar = x.Key.SalIdStAuxiliar,
                              AuxiliarUno = x.Key.SalIdStAuxiliarUno,
                              Centro = x.Key.SalIdStCentro,
                              Subcentro = x.Key.SalIdStSubcentro,
                              Nit = x.Key.SalIdStNit,
                              MovInValor = x.Sum(s => s.SalInValor)

                          }).ToList();

            return Ok(result);

        }

        [HttpGet("Informes/EstadoResultados/Movimientos/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeEstadoResultados(int asociado, string empresa, int ano, int mes)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes <= mes && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                              e.MovIdStNit
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              Nit = x.Key.MovIdStNit,
                              MovInValor = x.Sum(s => s.MovInValor)
                          }).ToList();

            return Ok(result);
        }

        [HttpGet("Informes/EstadoResultados/MovimientosMes/{asociado}/{empresa}/{ano}/{mes}")]
        public ActionResult InformeEstadoResultadosMes(int asociado, string empresa, int ano, int mes)
        {

            var result = (from ep in _context.Contmovimiento
                          join e in _context.Conmovimientos on new { ep1 = ep.TmoIdInAsociado, ep2 = ep.TmoIdStEmpresa, ep3 = ep.TmoIdStComprobante, ep4 = ep.TmoStNumero, ep5 = ep.TmoIdInConsecutivo } equals new { ep1 = e.MovIdInAsociado, ep2 = e.MovIdStEmpresa, ep3 = e.MovIdStComprobante, ep4 = e.MovStNumero, ep5 = e.MovIdInConsecutivo }
                          where ep.TmoIdInAsociado == asociado && ep.TmoIdStEmpresa == empresa && ep.TmoIdInAno == ano && ep.TmoIdInMes == mes && ep.TmoIdStAplicacion == "0"
                          group e by new
                          {
                              e.MovIdStCuenta,
                              e.MovIdStSubcuenta,
                              e.MovIdStAuxiliar,
                              e.MovIdStAuxiliarUno,
                              e.MovIdStCentro,
                              e.MovIdStSubcentro,
                              e.MovIdStNit,
                          } into x
                          select new
                          {
                              Cuenta = x.Key.MovIdStCuenta,
                              Subcuenta = x.Key.MovIdStSubcuenta,
                              Auxiliar = x.Key.MovIdStAuxiliar,
                              AuxiliarUno = x.Key.MovIdStAuxiliarUno,
                              Centro = x.Key.MovIdStCentro,
                              Subcentro = x.Key.MovIdStSubcentro,
                              Nit = x.Key.MovIdStNit,
                              MovInValor = x.Sum(s => s.MovInValor)

                          }).ToList();

            return Ok(result);

        }
    }
}
