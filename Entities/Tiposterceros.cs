﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Tiposterceros
    {
        public string TteIdStCodigo { get; set; }
        public string TteStNombre { get; set; }
        public string TteStClase { get; set; }
        public DateTime? TteDtFechaHora { get; set; }
        public string TteStUsuario { get; set; }
    }
}
