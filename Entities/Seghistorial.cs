﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Seghistorial
    {
        public int ObjectId { get; set; }
        public int HisIdInTabla { get; set; }
        public int HisIdInObjectId { get; set; }
        public DateTime HisDtFechaHora { get; set; }
        public string HisIdStUsuario { get; set; }
        public string HisIdStEstado { get; set; }
        public string HisStObservacion { get; set; }

        public virtual Segtablas HisIdInTablaNavigation { get; set; }
        public virtual Estados HisIdStEstadoNavigation { get; set; }
    }
}
