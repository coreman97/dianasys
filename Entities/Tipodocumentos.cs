﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Tipodocumentos
    {
        public Tipodocumentos()
        {
            Asociados = new HashSet<Asociados>();
            Clientes = new HashSet<Clientes>();
        }

        public string TdoIdStTipoDocumento { get; set; }
        public string TdoStDescripcion { get; set; }
        public string TdoStRequerido { get; set; }
        public string TdoStCodigoDian { get; set; }
        public string TdoStUsuario { get; set; }
        public DateTime? TdoDtFechaHora { get; set; }

        public virtual ICollection<Asociados> Asociados { get; set; }
        public virtual ICollection<Clientes> Clientes { get; set; }
    }
}
