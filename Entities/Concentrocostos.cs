﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Concentrocostos
    {
        public int CenIdInAsociado { get; set; }
        public string CenIdStEmpresa { get; set; }
        public string CenIdStCodigo { get; set; }
        public string CenStNombre { get; set; }
        public string CenIdStUsuario { get; set; }
        public DateTime? CenDtFechaHora { get; set; }

        public virtual Segusuarios CenIdStUsuarioNavigation { get; set; }
    }
}
