﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Concartilla
    {
        public int CarIdInAsociado { get; set; }
        public string CarIdStEmpresa { get; set; }
        public string CarIdStCuenta { get; set; }
        public string CarIdStSubcuenta { get; set; }
        public string CarIdStAuxiliar { get; set; }
        public string CarIdStAuxiliarUno { get; set; }
        public string CarStNombre { get; set; }
        public string CarStMovimiento { get; set; }
        public string CarStCentroCosto { get; set; }
        public string CarStManejaNit { get; set; }
        public string CarStNaturaleza { get; set; }
        public string CarStManejaBase { get; set; }
        public decimal? CarInPorcentajeRetencion { get; set; }
        public string CarStAjustable { get; set; }
        public string CarStIntegrado { get; set; }
        public string CarStVigencia { get; set; }
        public string CarStUtilidad { get; set; }
        public string CatIdStUsuario { get; set; }
        public DateTime? CarDtFechaHora { get; set; }

        public virtual Segusuarios CatIdStUsuarioNavigation { get; set; }
    }
}
