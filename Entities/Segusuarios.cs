﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segusuarios
    {
        public Segusuarios()
        {
            Asociados = new HashSet<Asociados>();
            Concartilla = new HashSet<Concartilla>();
            Concentrocostos = new HashSet<Concentrocostos>();
            Concomprobantes = new HashSet<Concomprobantes>();
            Contiposcomprobantes = new HashSet<Contiposcomprobantes>();
            Contmovimiento = new HashSet<Contmovimiento>();
            Empresas = new HashSet<Empresas>();
        }

        public string UsuStIdUsuario { get; set; }
        public int UsuIdInAsociado { get; set; }
        public string UsuIdStEmpresa { get; set; }
        public string UsuIdStNitCliente { get; set; }
        public string UsuStClave { get; set; }
        public string UsuStIdGrupoUsuario { get; set; }
        public string UsuStActivo { get; set; }
        public string UsuIdStUsuario { get; set; }
        public DateTime UsuDtFechaHora { get; set; }

        public virtual Seggrupousuarios UsuStIdGrupoUsuarioNavigation { get; set; }
        public virtual ICollection<Asociados> Asociados { get; set; }
        public virtual ICollection<Concartilla> Concartilla { get; set; }
        public virtual ICollection<Concentrocostos> Concentrocostos { get; set; }
        public virtual ICollection<Concomprobantes> Concomprobantes { get; set; }
        public virtual ICollection<Contiposcomprobantes> Contiposcomprobantes { get; set; }
        public virtual ICollection<Contmovimiento> Contmovimiento { get; set; }
        public virtual ICollection<Empresas> Empresas { get; set; }
    }
}
