﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Concomprobantes
    {
        public int ComIdInAsociado { get; set; }
        public string ComIdStEmpresa { get; set; }
        public string ComIdStCodigo { get; set; }
        public string ComStNombre { get; set; }
        public int? ComInNumeroInicial { get; set; }
        public int? ComInNumeroFinal { get; set; }
        public string ComStRepetirNumero { get; set; }
        public string ComStAutomatico { get; set; }
        public string ComStInterfaz { get; set; }
        public int? ComInNumero { get; set; }
        public string ComStOrigen { get; set; }
        public string ComIdStTipoComprobante { get; set; }
        public string ComStNiif { get; set; }
        public string ComIdStCuentaCaja { get; set; }
        public string ComIdStSubcuentaCaja { get; set; }
        public string ComIdStAuxiliarCaja { get; set; }
        public string ComIdStAuxiliarUnoCaja { get; set; }
        public string ComStPrefijo { get; set; }
        public string ComStFactuarcionElectronica { get; set; }
        public string ComIdStUsuario { get; set; }
        public DateTime ComDtFechaHora { get; set; }

        public virtual Contiposcomprobantes ComIdStTipoComprobanteNavigation { get; set; }
        public virtual Segusuarios ComIdStUsuarioNavigation { get; set; }
    }
}
