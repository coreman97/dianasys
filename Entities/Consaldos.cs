﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DianaSys.Entities
{
    public partial class Consaldos
    {
        public int SalIdInAsociado { get; set; }
        public int SalIdInConsecutivo { get; set; }
        public string SalIdStEmpresa { get; set; }
        public int SalIdInAno { get; set; }
        public int SalIdInMes { get; set; }
        public string SalIdsStAplicacion { get; set; }
        public string SalIdStCuenta { get; set; }
        public string SalIdStSubcuenta { get; set; }
        public string SalIdStAuxiliar { get; set; }
        public string SalIdStAuxiliarUno { get; set; }
        public string SalIdStCentro { get; set; }
        public string SalIdStSubcentro { get; set; }
        public string SalIdStNit { get; set; }
        public decimal SalInValor { get; set; }
        public DateTime SalDtFecha { get; set; }
        public string SalIdStUsuario { get; set; }
    }
}
