﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Seggrupousuarios
    {
        public Seggrupousuarios()
        {
            Segopcionesgrupo = new HashSet<Segopcionesgrupo>();
            Segusuarios = new HashSet<Segusuarios>();
        }

        public string GruIdStGrupoUsuario { get; set; }
        public string GruStDescripcion { get; set; }
        public string GruStActivo { get; set; }
        public string GruIdStUsuario { get; set; }
        public string GruDtFecha { get; set; }

        public virtual ICollection<Segopcionesgrupo> Segopcionesgrupo { get; set; }
        public virtual ICollection<Segusuarios> Segusuarios { get; set; }
    }
}
