﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Contipospuc
    {
        public Contipospuc()
        {
            Conplanunicocuenta = new HashSet<Conplanunicocuenta>();
        }

        public string TpuIdStCodigo { get; set; }
        public string TpuStNombre { get; set; }
        public string TpuIdStUsuario { get; set; }
        public DateTime? TpuDtFechaHora { get; set; }

        public virtual ICollection<Conplanunicocuenta> Conplanunicocuenta { get; set; }
    }
}
