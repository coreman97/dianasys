﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Conplanunicocuenta
    {
        public string PucIdStTipoPuc { get; set; }
        public string PucIdStCuenta { get; set; }
        public string PucStSubcuenta { get; set; }
        public string PucStAuxiliar { get; set; }
        public string PucStAuxiliarUno { get; set; }
        public string PucStNombre { get; set; }
        public string PucStMovimiento { get; set; }
        public string PucStCentroCosto { get; set; }
        public string PucSmanejatNit { get; set; }
        public string PucStNaturaleza { get; set; }
        public string PucStManejaBase { get; set; }
        public decimal? PucInPorcentajeRetencion { get; set; }
        public string PucStAjustable { get; set; }
        public string PucStIntegrado { get; set; }
        public string PucStVigencia { get; set; }
        public string PucStUtilidad { get; set; }
        public string PucStUsuario { get; set; }
        public DateTime? PucDtFechaHora { get; set; }
        public string Pucsmed { get; set; }
        public string Pucpart { get; set; }
        public string Puccpar { get; set; }
        public string Pucmagl { get; set; }
        public string Pucmagc { get; set; }
        public string Pucmags { get; set; }

        public virtual Contipospuc PucIdStTipoPucNavigation { get; set; }
    }
}
