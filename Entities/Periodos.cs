﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Periodos
    {
        public string PerIdStEmpresa { get; set; }
        public int PerIdInAsociado { get; set; }
        public int PerInAno { get; set; }
        public int PerInMes { get; set; }
        public string PerStAplicacion { get; set; }
        public string PerStEstado { get; set; }
        public string PerIdStUsuario { get; set; }
        public DateTime? PerDtFechaHora { get; set; }
    }
}
