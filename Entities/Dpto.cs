﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Dpto
    {
        public int DptIdInPais { get; set; }
        public int DptIdInCodigo { get; set; }
        public string DptStNombre { get; set; }
        public string DptUsuario { get; set; }
        public DateTime? DptDtFechaHora { get; set; }

        public virtual Pais DptIdInPaisNavigation { get; set; }
    }
}
