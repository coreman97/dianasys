﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segopciones
    {
        public Segopciones()
        {
            Segopcionesgrupo = new HashSet<Segopcionesgrupo>();
        }

        public string OpcIdStOpcion { get; set; }
        public string OpcStTipoOpcion { get; set; }
        public string OpcStDescripcion { get; set; }
        public string OpcStFormulario { get; set; }
        public string OpcIdStPadre { get; set; }
        public string OpcStAdicionar { get; set; }
        public string OpcStModificar { get; set; }
        public string OpcStEliminar { get; set; }
        public string OpcStImprimir { get; set; }
        public string OpcStConsultar { get; set; }
        public decimal? OpcIdInNivel { get; set; }
        public decimal? OpcInOrden { get; set; }
        public decimal? OpcIdStImagen { get; set; }

        public virtual Segnivelopcion OpcIdInNivelNavigation { get; set; }
        public virtual Segimagenes OpcIdStImagenNavigation { get; set; }
        public virtual ICollection<Segopcionesgrupo> Segopcionesgrupo { get; set; }
    }
}
