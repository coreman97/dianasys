﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Estados
    {
        public Estados()
        {
            Seghistorial = new HashSet<Seghistorial>();
        }

        public string EtaIdStEstado { get; set; }
        public string EtaStDescripcion { get; set; }

        public virtual ICollection<Seghistorial> Seghistorial { get; set; }
    }
}
