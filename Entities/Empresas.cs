﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Empresas
    {
        public int EmpIdInAsociado { get; set; }
        public string EmpIdStCodigo { get; set; }
        public string EmpStNit { get; set; }
        public string EmpStNombre { get; set; }
        public string EmpStDireccion { get; set; }
        public int? EmpIdInPaisLocalizacion { get; set; }
        public int? EmpIdInDptoLocalizacion { get; set; }
        public int? EmpIdInMuniLocalizacion { get; set; }
        public string EmpStEstado { get; set; }
        public string EmpStSigla { get; set; }
        public string EmpStAbreviatura { get; set; }
        public string EmpIdStTipoEmpresa { get; set; }
        public string EmpStObjetoSocial { get; set; }
        public DateTime EmpDtFechaConstitucion { get; set; }
        public string EmpStMatriculaMercantil { get; set; }
        public string EmpStNotaria { get; set; }
        public string EmpStNumeroEscritura { get; set; }
        public DateTime EmpDtFechaRegistro { get; set; }
        public string EmpStLibroRegistro { get; set; }
        public int EmpInDuracion { get; set; }
        public string EmpStRegimen { get; set; }
        public string EmpIdStRepresentanteLegal { get; set; }
        public string EmpIdStContador { get; set; }
        public string EmpStTarjetaContador { get; set; }
        public string EmpIdStRevisorFiscal { get; set; }
        public string EmpStTarjetaRevisorFiscal { get; set; }
        public int? EmpInAnoContabilidad { get; set; }
        public int? EmpInMescontabilidad { get; set; }
        public string EmpIdStUsuario { get; set; }
        public DateTime? EmpDtFechaHora { get; set; }

        public virtual Asociados EmpIdInAsociadoNavigation { get; set; }
        public virtual Segusuarios EmpIdStUsuarioNavigation { get; set; }
    }
}
