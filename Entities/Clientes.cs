﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Clientes
    {
        public int CliIdInAsociado { get; set; }
        public string CliIdStEmpresa { get; set; }
        public string CliIdStNit { get; set; }
        public string CliStDigito { get; set; }
        public string CliIdStipoDocumento { get; set; }
        public string CliStNombreCliente { get; set; }
        public string CliStPrimerNombre { get; set; }
        public string CliStSegundoNombre { get; set; }
        public string CliStPrimerApellido { get; set; }
        public string CliStSegundoApellido { get; set; }
        public string CliStDireccion { get; set; }
        public int? CliInPaisLocalizacion { get; set; }
        public int? CliInDptoLocalizacion { get; set; }
        public int? CliInMuniLocalizacion { get; set; }
        public string CliStTelefono { get; set; }
        public string CliStCiudad { get; set; }
        public string CliStCelular { get; set; }
        public string CliStCorreoElectronico { get; set; }
        public string CliStRetefuente { get; set; }
        public string CliStAutoretenedor { get; set; }
        public string CliStRenta { get; set; }
        public string CliStGranContribuyente { get; set; }
        public string CliStResponsableIva { get; set; }
        public string CliStDeclarante { get; set; }
        public string CliStRegimenTributario { get; set; }
        public string ClirStResidencia { get; set; }
        public string CliStVigencia { get; set; }
        public decimal? CliInCupoCredito { get; set; }
        public decimal? CliInDiasGracia { get; set; }
        public string CliStApartadoAereo { get; set; }
        public string CliStVendedorAsignado { get; set; }
        public string CliStCliRetencionIva { get; set; }
        public string CliStCliRetecionRenta { get; set; }
        public string CliStCliRetencionIca { get; set; }
        public string CliStProRetencionIva { get; set; }
        public string CliStProRetencionRenta { get; set; }
        public string CliStProRetencionIca { get; set; }
        public string CliStSexo { get; set; }
        public string CliIdStUsuario { get; set; }
        public DateTime? CliDtFechaHora { get; set; }

        public virtual Tipodocumentos CliIdStipoDocumentoNavigation { get; set; }
    }
}
