﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Contiposcomprobantes
    {
        public Contiposcomprobantes()
        {
            Concomprobantes = new HashSet<Concomprobantes>();
        }

        public string TcpIdStTipoComprobante { get; set; }
        public string TcpStDescripcion { get; set; }
        public string TcpIdStUsuario { get; set; }
        public DateTime? TcpDtFechaHora { get; set; }

        public virtual Segusuarios TcpIdStUsuarioNavigation { get; set; }
        public virtual ICollection<Concomprobantes> Concomprobantes { get; set; }
    }
}
