﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Municipio
    {
        public int MunIdInPais { get; set; }
        public int MunIdInDpto { get; set; }
        public int MunIdInCodigo { get; set; }
        public string MunStNombre { get; set; }
        public string MunStUusario { get; set; }
        public DateTime? MunDtFechaHora { get; set; }
    }
}
