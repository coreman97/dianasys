﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DianaSys.Entities
{
    public partial class bddianasis_onlineContext : DbContext
    {
        public bddianasis_onlineContext()
        {
        }

        public bddianasis_onlineContext(DbContextOptions<bddianasis_onlineContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Asociados> Asociados { get; set; }
        public virtual DbSet<Clientes> Clientes { get; set; }
        public virtual DbSet<Concartilla> Concartilla { get; set; }
        public virtual DbSet<Concentrocostos> Concentrocostos { get; set; }
        public virtual DbSet<Concomprobantes> Concomprobantes { get; set; }
        public virtual DbSet<Conplanunicocuenta> Conplanunicocuenta { get; set; }
        public virtual DbSet<Consubcentrocostos> Consubcentrocostos { get; set; }
        public virtual DbSet<Contiposcomprobantes> Contiposcomprobantes { get; set; }
        public virtual DbSet<Contipospuc> Contipospuc { get; set; }
        public virtual DbSet<Contmovimiento> Contmovimiento { get; set; }
        public virtual DbSet<Dpto> Dpto { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<Estados> Estados { get; set; }
        public virtual DbSet<Municipio> Municipio { get; set; }
        public virtual DbSet<Pais> Pais { get; set; }
        public virtual DbSet<Periodos> Periodos { get; set; }
        public virtual DbSet<Seggrupousuarios> Seggrupousuarios { get; set; }
        public virtual DbSet<Seghistorial> Seghistorial { get; set; }
        public virtual DbSet<Segimagenes> Segimagenes { get; set; }
        public virtual DbSet<Segnivelopcion> Segnivelopcion { get; set; }
        public virtual DbSet<Segopciones> Segopciones { get; set; }
        public virtual DbSet<Segopcionesgrupo> Segopcionesgrupo { get; set; }
        public virtual DbSet<Segtablas> Segtablas { get; set; }
        public virtual DbSet<Segusuarios> Segusuarios { get; set; }
        public virtual DbSet<Tipodocumentos> Tipodocumentos { get; set; }
        public virtual DbSet<Tiposterceros> Tiposterceros { get; set; }
        public virtual DbSet<Conmovimientos> Conmovimientos { get; set; }
        public virtual DbSet<RTipoclientes> RTipoclientes { get; set; }

        public virtual DbSet<Consaldos> Consaldos { get; set; }
        // Unable to generate entity type for table 'bddianasis_online.conmovimientos'. Please see the warning messages.
        // Unable to generate entity type for table 'bddianasis_online.consaldos'. Please see the warning messages.
        // Unable to generate entity type for table 'bddianasis_online.rtipoclientes'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Consaldos>(entity =>
            {
                entity.HasKey(e => e.SalIdInConsecutivo);
                entity.ToTable("consaldos", "bddianasis_online");
            });

            modelBuilder.Entity<Asociados>(entity =>
            {
                entity.HasKey(e => e.AflIdInCodigo);

                entity.ToTable("asociados", "bddianasis_online");

                entity.HasIndex(e => e.AflIdStTipoDocumento)
                    .HasName("AflIdStTipoDocumento");

                entity.HasIndex(e => e.AflIdStUsuario)
                    .HasName("AflIdStUsuario");

                entity.HasIndex(e => new { e.AflIdInPaisLocalizacion, e.AflIdInDptoLocalizacion, e.AflIdinMunicipioLocalizacion })
                    .HasName("AflIdInPaisLocalizacion");

                entity.Property(e => e.AflIdInCodigo)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AflDtFechaAfiliacion).HasColumnType("date");

                entity.Property(e => e.AflDtFechaRetiro).HasColumnType("date");

                entity.Property(e => e.AflIdInDptoLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.AflIdInPaisLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.AflIdStEstado)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.AflIdStIdentificacion)
                    .IsRequired()
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.AflIdStTipoDocumento)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.AflIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AflIdStUsuarioAprobo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AflIdinMunicipioLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.AflStCelular)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.AflStCorreoElectronico)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AflStDireccion)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AflStNombreAfiliado)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.AflStTelefono)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.AflIdStTipoDocumentoNavigation)
                    .WithMany(p => p.Asociados)
                    .HasForeignKey(d => d.AflIdStTipoDocumento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Asociados_TipoDocumentos");

                entity.HasOne(d => d.AflIdStUsuarioNavigation)
                    .WithMany(p => p.Asociados)
                    .HasForeignKey(d => d.AflIdStUsuario)
                    .HasConstraintName("FK_Asociados_SegUsuarios");
            });

            modelBuilder.Entity<Clientes>(entity =>
            {
                entity.HasKey(e => new { e.CliIdInAsociado, e.CliIdStEmpresa, e.CliIdStNit });

                entity.ToTable("clientes", "bddianasis_online");

                entity.HasIndex(e => e.CliIdStipoDocumento)
                    .HasName("CliIdSTipoDocumento");

                entity.HasIndex(e => new { e.CliIdInAsociado, e.CliIdStEmpresa })
                    .HasName("CliIdInAsociado");

                entity.HasIndex(e => new { e.CliInPaisLocalizacion, e.CliInDptoLocalizacion, e.CliInMuniLocalizacion })
                    .HasName("CliInPaisLocalizacion");

                entity.Property(e => e.CliIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.CliIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.CliIdStNit)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.CliIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CliIdStipoDocumento)
                    .IsRequired()
                    .HasColumnName("CliIdSTipoDocumento")
                    .HasColumnType("char(2)");

                entity.Property(e => e.CliInCupoCredito).HasColumnType("decimal(11,0)");

                entity.Property(e => e.CliInDiasGracia).HasColumnType("decimal(3,0)");

                entity.Property(e => e.CliInDptoLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.CliInMuniLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.CliInPaisLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.CliStApartadoAereo)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CliStAutoretenedor)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStCelular)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CliStCiudad)
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.CliStCliRetecionRenta).HasColumnType("char(1)");

                entity.Property(e => e.CliStCliRetencionIca).HasColumnType("char(1)");

                entity.Property(e => e.CliStCliRetencionIva).HasColumnType("char(1)");

                entity.Property(e => e.CliStCorreoElectronico)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CliStDeclarante).HasColumnType("char(1)");

                entity.Property(e => e.CliStDigito).HasColumnType("char(1)");

                entity.Property(e => e.CliStDireccion)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CliStGranContribuyente)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStNombreCliente)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CliStPrimerApellido)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CliStPrimerNombre)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CliStProRetencionIca).HasColumnType("char(1)");

                entity.Property(e => e.CliStProRetencionIva).HasColumnType("char(1)");

                entity.Property(e => e.CliStProRetencionRenta).HasColumnType("char(1)");

                entity.Property(e => e.CliStRegimenTributario)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStRenta)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStResponsableIva)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStRetefuente)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CliStSegundoApellido)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CliStSegundoNombre)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CliStSexo).HasColumnType("char(1)");

                entity.Property(e => e.CliStTelefono)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CliStVendedorAsignado)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.CliStVigencia)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.ClirStResidencia)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.CliIdStipoDocumentoNavigation)
                    .WithMany(p => p.Clientes)
                    .HasForeignKey(d => d.CliIdStipoDocumento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Clientes_TipoDocumentos");
            });

            modelBuilder.Entity<Concartilla>(entity =>
            {
                entity.HasKey(e => new { e.CarIdInAsociado, e.CarIdStEmpresa, e.CarIdStCuenta, e.CarIdStSubcuenta, e.CarIdStAuxiliar, e.CarIdStAuxiliarUno });

                entity.ToTable("concartilla", "bddianasis_online");

                entity.HasIndex(e => e.CatIdStUsuario)
                    .HasName("CatIdStUsuario");

                entity.HasIndex(e => new { e.CarIdInAsociado, e.CarIdStEmpresa })
                    .HasName("CarIdInAsociado");

                entity.Property(e => e.CarIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.CarIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.CarIdStCuenta).HasColumnType("char(4)");

                entity.Property(e => e.CarIdStSubcuenta).HasColumnType("char(2)");

                entity.Property(e => e.CarIdStAuxiliar).HasColumnType("char(2)");

                entity.Property(e => e.CarIdStAuxiliarUno).HasColumnType("char(2)");

                entity.Property(e => e.CarInPorcentajeRetencion).HasColumnType("decimal(4,1)");

                entity.Property(e => e.CarStAjustable)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStCentroCosto)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStIntegrado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStManejaBase)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStManejaNit)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStMovimiento)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStNaturaleza)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CarStNombre)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CarStUtilidad).HasColumnType("char(1)");

                entity.Property(e => e.CarStVigencia)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.CatIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.CatIdStUsuarioNavigation)
                    .WithMany(p => p.Concartilla)
                    .HasForeignKey(d => d.CatIdStUsuario)
                    .HasConstraintName("FK_ConCartilla_SegUsuarios");
            });

            modelBuilder.Entity<Concentrocostos>(entity =>
            {
                entity.HasKey(e => new { e.CenIdInAsociado, e.CenIdStEmpresa, e.CenIdStCodigo });

                entity.ToTable("concentrocostos", "bddianasis_online");

                entity.HasIndex(e => e.CenIdStUsuario)
                    .HasName("CenIdStUsuario");

                entity.HasIndex(e => new { e.CenIdInAsociado, e.CenIdStEmpresa })
                    .HasName("CenIdInAsociado");

                entity.Property(e => e.CenIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.CenIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.CenIdStCodigo).HasColumnType("char(2)");

                entity.Property(e => e.CenIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CenStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.CenIdStUsuarioNavigation)
                    .WithMany(p => p.Concentrocostos)
                    .HasForeignKey(d => d.CenIdStUsuario)
                    .HasConstraintName("FK_concentrocostos_segusuarios");
            });

            modelBuilder.Entity<Concomprobantes>(entity =>
            {
                entity.HasKey(e => new { e.ComIdInAsociado, e.ComIdStEmpresa, e.ComIdStCodigo });

                entity.ToTable("concomprobantes", "bddianasis_online");

                entity.HasIndex(e => e.ComIdStTipoComprobante)
                    .HasName("ComIdStTipoComprobante");

                entity.HasIndex(e => e.ComIdStUsuario)
                    .HasName("ComIdStUsuario");

                entity.HasIndex(e => new { e.ComIdInAsociado, e.ComIdStEmpresa })
                    .HasName("ComIdInAsociado");

                entity.Property(e => e.ComIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.ComIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStCodigo).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStAuxiliarCaja).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStAuxiliarUnoCaja).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStCuentaCaja).HasColumnType("char(4)");

                entity.Property(e => e.ComIdStSubcuentaCaja).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStTipoComprobante).HasColumnType("char(2)");

                entity.Property(e => e.ComIdStUsuario)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ComInNumero).HasColumnType("int(11)");

                entity.Property(e => e.ComInNumeroFinal).HasColumnType("int(11)");

                entity.Property(e => e.ComInNumeroInicial).HasColumnType("int(11)");

                entity.Property(e => e.ComStAutomatico)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.ComStFactuarcionElectronica).HasColumnType("char(1)");

                entity.Property(e => e.ComStInterfaz).HasColumnType("char(1)");

                entity.Property(e => e.ComStNiif).HasColumnType("char(1)");

                entity.Property(e => e.ComStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ComStOrigen).HasColumnType("char(1)");

                entity.Property(e => e.ComStPrefijo).HasColumnType("char(6)");

                entity.Property(e => e.ComStRepetirNumero)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.ComIdStTipoComprobanteNavigation)
                    .WithMany(p => p.Concomprobantes)
                    .HasForeignKey(d => d.ComIdStTipoComprobante)
                    .HasConstraintName("FK_ConComprobantes_ConTiposComprobantes");

                entity.HasOne(d => d.ComIdStUsuarioNavigation)
                    .WithMany(p => p.Concomprobantes)
                    .HasForeignKey(d => d.ComIdStUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_concomprobantes_segusuarios");
            });

            modelBuilder.Entity<Conplanunicocuenta>(entity =>
            {
                entity.HasKey(e => new { e.PucIdStTipoPuc, e.PucIdStCuenta, e.PucStSubcuenta, e.PucStAuxiliar, e.PucStAuxiliarUno });

                entity.ToTable("conplanunicocuenta", "bddianasis_online");

                entity.HasIndex(e => e.PucIdStTipoPuc)
                    .HasName("PucIdStTipoPuc");

                entity.Property(e => e.PucIdStTipoPuc).HasColumnType("char(2)");

                entity.Property(e => e.PucIdStCuenta).HasColumnType("char(4)");

                entity.Property(e => e.PucStSubcuenta).HasColumnType("char(2)");

                entity.Property(e => e.PucStAuxiliar).HasColumnType("char(2)");

                entity.Property(e => e.PucStAuxiliarUno).HasColumnType("char(2)");

                entity.Property(e => e.PucInPorcentajeRetencion).HasColumnType("decimal(4,1)");

                entity.Property(e => e.PucSmanejatNit)
                    .IsRequired()
                    .HasColumnName("PucSManejatNit")
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStAjustable)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStCentroCosto)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStIntegrado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStManejaBase)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStMovimiento)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStNaturaleza)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.PucStNombre)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PucStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PucStUtilidad).HasColumnType("char(1)");

                entity.Property(e => e.PucStVigencia)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Puccpar)
                    .HasColumnName("puccpar")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pucmagc)
                    .HasColumnName("pucmagc")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pucmagl)
                    .HasColumnName("pucmagl")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pucmags)
                    .HasColumnName("pucmags")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pucpart)
                    .HasColumnName("pucpart")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Pucsmed)
                    .HasColumnName("pucsmed")
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.PucIdStTipoPucNavigation)
                    .WithMany(p => p.Conplanunicocuenta)
                    .HasForeignKey(d => d.PucIdStTipoPuc)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PlanUnicoCuenta_TiposPuc");
            });

            modelBuilder.Entity<RTipoclientes>(entity =>
            {
                entity.HasKey(e => new { e.RtiIdStAsociado, e.RtiIdStEmpresa, e.RtiIdStNitCliente, e.RtiIdStTipoCliente });
                entity.ToTable("rTipoclientes", "bddianasis_online");

                entity.Property(e => e.RtiIdStEmpresa).HasColumnType("char(2)");
                entity.Property(e => e.RtiIdStTipoCliente).HasColumnType("char(2)");
                entity.Property(e => e.RtiIdStNitCliente).HasColumnType("char(14)");
            });

            modelBuilder.Entity<Consubcentrocostos>(entity =>
            {
                entity.HasKey(e => new { e.SceIdInAsociado, e.SceIdStEmpresa, e.SceIdStCentro, e.SceIdStCodigo });

                entity.ToTable("consubcentrocostos", "bddianasis_online");

                entity.HasIndex(e => new { e.SceIdInAsociado, e.SceIdStEmpresa, e.SceIdStCentro })
                    .HasName("SceIdInAsociado");

                entity.Property(e => e.SceIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.SceIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.SceIdStCentro).HasColumnType("char(2)");

                entity.Property(e => e.SceIdStCodigo).HasColumnType("char(6)");

                entity.Property(e => e.SceIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SceStEstado).HasColumnType("char(1)");

                entity.Property(e => e.SceStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Contiposcomprobantes>(entity =>
            {
                entity.HasKey(e => e.TcpIdStTipoComprobante);

                entity.ToTable("contiposcomprobantes", "bddianasis_online");

                entity.HasIndex(e => e.TcpIdStUsuario)
                    .HasName("TcpIdStUsuario");

                entity.Property(e => e.TcpIdStTipoComprobante)
                    .HasColumnType("char(2)")
                    .ValueGeneratedNever();

                entity.Property(e => e.TcpIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TcpStDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.TcpIdStUsuarioNavigation)
                    .WithMany(p => p.Contiposcomprobantes)
                    .HasForeignKey(d => d.TcpIdStUsuario)
                    .HasConstraintName("FK_contiposcomprobantes_segusuarios");
            });

            modelBuilder.Entity<Contipospuc>(entity =>
            {
                entity.HasKey(e => e.TpuIdStCodigo);

                entity.ToTable("contipospuc", "bddianasis_online");

                entity.Property(e => e.TpuIdStCodigo)
                    .HasColumnType("char(2)")
                    .ValueGeneratedNever();

                entity.Property(e => e.TpuIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TpuStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Conmovimientos>(entity =>
            {
                entity.HasKey(e => e.MovInItem);
                entity.ToTable("conmovimientos", "bddianasis_online");
            });
            
            modelBuilder.Entity<Contmovimiento>(entity =>
            {
                entity.HasKey(e => new { e.TmoIdStEmpresa, e.TmoIdInAsociado, e.TmoIdStComprobante, e.TmoStNumero });

                entity.ToTable("contmovimiento", "bddianasis_online");

                entity.HasIndex(e => e.TmoIdStUsuario)
                    .HasName("TmoIdStUsuario");

                entity.HasIndex(e => new { e.TmoIdInAsociado, e.TmoIdStEmpresa })
                    .HasName("TmoIdInAsociado_2");

                entity.HasIndex(e => new { e.TmoIdInAsociado, e.TmoIdStEmpresa, e.TmoIdStComprobante })
                    .HasName("TmoIdInAsociado");

                entity.HasIndex(e => new { e.TmoIdStEmpresa, e.TmoIdInAsociado, e.TmoIdInAno, e.TmoIdInMes, e.TmoIdStAplicacion })
                    .HasName("TmoIdStEmpresa");

                entity.Property(e => e.TmoIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.TmoIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.TmoIdStComprobante).HasColumnType("char(2)");

                entity.Property(e => e.TmoStNumero)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.TmoDtFecha).HasColumnType("date");

                entity.Property(e => e.TmoIdInAno).HasColumnType("int(11)");

                entity.Property(e => e.TmoIdInConsecutivo).HasColumnType("int(11)");

                entity.Property(e => e.TmoIdInMes).HasColumnType("int(11)");

                entity.Property(e => e.TmoIdStAplicacion)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.TmoIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.TmoStDescripcion)
                    .IsRequired()
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.HasOne(d => d.TmoIdStUsuarioNavigation)
                    .WithMany(p => p.Contmovimiento)
                    .HasForeignKey(d => d.TmoIdStUsuario)
                    .HasConstraintName("FK_ConTmovimiento_SegUsuarios");
            });

            modelBuilder.Entity<Dpto>(entity =>
            {
                entity.HasKey(e => new { e.DptIdInPais, e.DptIdInCodigo });

                entity.ToTable("dpto", "bddianasis_online");

                entity.HasIndex(e => e.DptIdInPais)
                    .HasName("DptIdInPais");

                entity.Property(e => e.DptIdInPais).HasColumnType("int(11)");

                entity.Property(e => e.DptIdInCodigo).HasColumnType("int(11)");

                entity.Property(e => e.DptStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DptUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.DptIdInPaisNavigation)
                    .WithMany(p => p.Dpto)
                    .HasForeignKey(d => d.DptIdInPais)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Dpto_Pais");
            });

            modelBuilder.Entity<Empresas>(entity =>
            {
                entity.HasKey(e => new { e.EmpIdInAsociado, e.EmpIdStCodigo });

                entity.ToTable("empresas", "bddianasis_online");

                entity.HasIndex(e => e.EmpIdInAsociado)
                    .HasName("EmpIdInAsociado");

                entity.HasIndex(e => e.EmpIdStUsuario)
                    .HasName("EmpIdStUsuario");

                entity.HasIndex(e => new { e.EmpIdInAsociado, e.EmpIdStCodigo, e.EmpIdStContador })
                    .HasName("EmpIdInAsociado_3");

                entity.HasIndex(e => new { e.EmpIdInAsociado, e.EmpIdStCodigo, e.EmpIdStRepresentanteLegal })
                    .HasName("EmpIdInAsociado_2");

                entity.HasIndex(e => new { e.EmpIdInPaisLocalizacion, e.EmpIdInDptoLocalizacion, e.EmpIdInMuniLocalizacion })
                    .HasName("EmpIdInPaisLocalizacion");

                entity.Property(e => e.EmpIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.EmpIdStCodigo).HasColumnType("char(2)");

                entity.Property(e => e.EmpDtFechaConstitucion).HasColumnType("date");

                entity.Property(e => e.EmpDtFechaRegistro).HasColumnType("date");

                entity.Property(e => e.EmpIdInDptoLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.EmpIdInMuniLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.EmpIdInPaisLocalizacion).HasColumnType("int(11)");

                entity.Property(e => e.EmpIdStContador)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.EmpIdStRepresentanteLegal)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.EmpIdStRevisorFiscal)
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.EmpIdStTipoEmpresa).HasColumnType("char(1)");

                entity.Property(e => e.EmpIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmpInAnoContabilidad).HasColumnType("int(11)");

                entity.Property(e => e.EmpInDuracion).HasColumnType("int(11)");

                entity.Property(e => e.EmpInMescontabilidad).HasColumnType("int(11)");

                entity.Property(e => e.EmpStAbreviatura).HasColumnType("char(5)");

                entity.Property(e => e.EmpStDireccion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStEstado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.EmpStLibroRegistro)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStMatriculaMercantil)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStNit)
                    .IsRequired()
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStNotaria)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStNumeroEscritura)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStObjetoSocial)
                    .IsRequired()
                    .HasMaxLength(254)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStRegimen)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStSigla)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStTarjetaContador)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.EmpStTarjetaRevisorFiscal)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.HasOne(d => d.EmpIdInAsociadoNavigation)
                    .WithMany(p => p.Empresas)
                    .HasForeignKey(d => d.EmpIdInAsociado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Empresas_Asociados");

                entity.HasOne(d => d.EmpIdStUsuarioNavigation)
                    .WithMany(p => p.Empresas)
                    .HasForeignKey(d => d.EmpIdStUsuario)
                    .HasConstraintName("FK_Empresas_SegUsuarios");
            });

            modelBuilder.Entity<Estados>(entity =>
            {
                entity.HasKey(e => e.EtaIdStEstado);

                entity.ToTable("estados", "bddianasis_online");

                entity.Property(e => e.EtaIdStEstado)
                    .HasColumnType("char(1)")
                    .ValueGeneratedNever();

                entity.Property(e => e.EtaStDescripcion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Municipio>(entity =>
            {
                entity.HasKey(e => new { e.MunIdInPais, e.MunIdInDpto, e.MunIdInCodigo });

                entity.ToTable("municipio", "bddianasis_online");

                entity.HasIndex(e => new { e.MunIdInPais, e.MunIdInDpto })
                    .HasName("MunIdInPais");

                entity.Property(e => e.MunIdInPais).HasColumnType("int(11)");

                entity.Property(e => e.MunIdInDpto).HasColumnType("int(11)");

                entity.Property(e => e.MunIdInCodigo).HasColumnType("int(11)");

                entity.Property(e => e.MunStNombre)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.MunStUusario)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Pais>(entity =>
            {
                entity.HasKey(e => e.PaiIdInCodigo);

                entity.ToTable("pais", "bddianasis_online");

                entity.Property(e => e.PaiIdInCodigo)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.PaiStCodigoDian)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PaiStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PaiStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Periodos>(entity =>
            {
                entity.HasKey(e => new { e.PerIdStEmpresa, e.PerIdInAsociado, e.PerInAno, e.PerInMes, e.PerStAplicacion });

                entity.ToTable("periodos", "bddianasis_online");

                entity.Property(e => e.PerIdStEmpresa).HasColumnType("char(2)");

                entity.Property(e => e.PerIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.PerInAno).HasColumnType("int(11)");

                entity.Property(e => e.PerInMes).HasColumnType("int(11)");

                entity.Property(e => e.PerStAplicacion).HasColumnType("char(1)");

                entity.Property(e => e.PerIdStUsuario)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PerStEstado)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Seggrupousuarios>(entity =>
            {
                entity.HasKey(e => e.GruIdStGrupoUsuario);

                entity.ToTable("seggrupousuarios", "bddianasis_online");

                entity.Property(e => e.GruIdStGrupoUsuario)
                    .HasColumnType("char(4)")
                    .ValueGeneratedNever();

                entity.Property(e => e.GruDtFecha)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GruIdStUsuario)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GruStActivo)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.GruStDescripcion)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Seghistorial>(entity =>
            {
                entity.HasKey(e => e.ObjectId);

                entity.ToTable("seghistorial", "bddianasis_online");

                entity.HasIndex(e => e.HisIdInTabla)
                    .HasName("HisIdInTabla");

                entity.HasIndex(e => e.HisIdStEstado)
                    .HasName("HisIdStEstado");

                entity.Property(e => e.ObjectId)
                    .HasColumnName("ObjectID")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.HisIdInObjectId)
                    .HasColumnName("HisIdInObjectID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.HisIdInTabla).HasColumnType("int(11)");

                entity.Property(e => e.HisIdStEstado)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.HisIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HisStObservacion).IsUnicode(false);

                entity.HasOne(d => d.HisIdInTablaNavigation)
                    .WithMany(p => p.Seghistorial)
                    .HasForeignKey(d => d.HisIdInTabla)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Historial_Tablas");

                entity.HasOne(d => d.HisIdStEstadoNavigation)
                    .WithMany(p => p.Seghistorial)
                    .HasForeignKey(d => d.HisIdStEstado)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Historial_Estados");
            });

            modelBuilder.Entity<Segimagenes>(entity =>
            {
                entity.HasKey(e => e.ImgInIdImagen);

                entity.ToTable("segimagenes", "bddianasis_online");

                entity.Property(e => e.ImgInIdImagen).HasColumnType("decimal(2,0)");

                entity.Property(e => e.ImgStDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Segnivelopcion>(entity =>
            {
                entity.HasKey(e => e.NopInIdNivelOpcion);

                entity.ToTable("segnivelopcion", "bddianasis_online");

                entity.Property(e => e.NopInIdNivelOpcion).HasColumnType("decimal(2,0)");

                entity.Property(e => e.NopStDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Segopciones>(entity =>
            {
                entity.HasKey(e => e.OpcIdStOpcion);

                entity.ToTable("segopciones", "bddianasis_online");

                entity.HasIndex(e => e.OpcIdInNivel)
                    .HasName("OpcIdInNivel");

                entity.HasIndex(e => e.OpcIdStImagen)
                    .HasName("OpcIdStImagen");

                entity.Property(e => e.OpcIdStOpcion)
                    .HasColumnType("char(4)")
                    .ValueGeneratedNever();

                entity.Property(e => e.OpcIdInNivel).HasColumnType("decimal(2,0)");

                entity.Property(e => e.OpcIdStImagen).HasColumnType("decimal(2,0)");

                entity.Property(e => e.OpcIdStPadre)                    
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.OpcInOrden).HasColumnType("decimal(4,0)");

                entity.Property(e => e.OpcStAdicionar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.OpcStConsultar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.OpcStDescripcion)
                    .IsRequired()
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.OpcStEliminar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.OpcStFormulario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OpcStImprimir)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.OpcStModificar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.OpcStTipoOpcion)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.OpcIdInNivelNavigation)
                    .WithMany(p => p.Segopciones)
                    .HasForeignKey(d => d.OpcIdInNivel)
                    .HasConstraintName("FK_SegOpciones_SegNivelOpcion");

                entity.HasOne(d => d.OpcIdStImagenNavigation)
                    .WithMany(p => p.Segopciones)
                    .HasForeignKey(d => d.OpcIdStImagen)
                    .HasConstraintName("FK_SegOpciones_SegImagenes");
            });

            modelBuilder.Entity<Segopcionesgrupo>(entity =>
            {
                entity.HasKey(e => new { e.RgrIdStGrupoUsuario, e.RgrIdStOpcion });

                entity.ToTable("segopcionesgrupo", "bddianasis_online");

                entity.HasIndex(e => e.RgrIdStGrupoUsuario)
                    .HasName("RgrIdStGrupoUsuario");

                entity.HasIndex(e => e.RgrIdStOpcion)
                    .HasName("RgrIdStOpcion");

                entity.Property(e => e.RgrIdStGrupoUsuario).HasColumnType("char(4)");

                entity.Property(e => e.RgrIdStOpcion).HasColumnType("char(4)");

                entity.Property(e => e.RgrIdStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RgrStAdicionar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.RgrStConsultar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.RgrStEliminar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.RgrStImprimir)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.RgrStModificar)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.HasOne(d => d.RgrIdStGrupoUsuarioNavigation)
                    .WithMany(p => p.Segopcionesgrupo)
                    .HasForeignKey(d => d.RgrIdStGrupoUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SegOpcionesGrupo_SegGrupoUsuarios");

                entity.HasOne(d => d.RgrIdStOpcionNavigation)
                    .WithMany(p => p.Segopcionesgrupo)
                    .HasForeignKey(d => d.RgrIdStOpcion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SegOpcionesGrupo_SegOpciones");
            });

            modelBuilder.Entity<Segtablas>(entity =>
            {
                entity.HasKey(e => e.TabIdInTabla);

                entity.ToTable("segtablas", "bddianasis_online");

                entity.Property(e => e.TabIdInTabla)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.TabStDescripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Segusuarios>(entity =>
            {
                entity.HasKey(e => e.UsuStIdUsuario);

                entity.ToTable("segusuarios", "bddianasis_online");

                entity.HasIndex(e => e.UsuStIdGrupoUsuario)
                    .HasName("UsuStIdGrupoUsuario");

                entity.Property(e => e.UsuStIdUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.UsuIdInAsociado).HasColumnType("int(11)");

                entity.Property(e => e.UsuIdStEmpresa)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.UsuIdStNitCliente)
                    .IsRequired()
                    .HasMaxLength(14)
                    .IsUnicode(false);

                entity.Property(e => e.UsuIdStUsuario)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.UsuStActivo)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.UsuStClave)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.UsuStIdGrupoUsuario)
                    .IsRequired()
                    .HasColumnType("char(4)");

                entity.HasOne(d => d.UsuStIdGrupoUsuarioNavigation)
                    .WithMany(p => p.Segusuarios)
                    .HasForeignKey(d => d.UsuStIdGrupoUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SegUsuarios_SegGrupoUsuarios");
            });

            modelBuilder.Entity<Tipodocumentos>(entity =>
            {
                entity.HasKey(e => e.TdoIdStTipoDocumento);

                entity.ToTable("tipodocumentos", "bddianasis_online");

                entity.Property(e => e.TdoIdStTipoDocumento)
                    .HasColumnType("char(2)")
                    .ValueGeneratedNever();

                entity.Property(e => e.TdoStCodigoDian).HasColumnType("char(2)");

                entity.Property(e => e.TdoStDescripcion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TdoStRequerido)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.TdoStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Tiposterceros>(entity =>
            {
                entity.HasKey(e => e.TteIdStCodigo);

                entity.ToTable("tiposterceros", "bddianasis_online");

                entity.Property(e => e.TteIdStCodigo)
                    .HasColumnType("char(2)")
                    .ValueGeneratedNever();

                entity.Property(e => e.TteStClase).HasColumnType("char(1)");

                entity.Property(e => e.TteStNombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TteStUsuario)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
        }
    }
}
