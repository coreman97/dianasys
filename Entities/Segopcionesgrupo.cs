﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segopcionesgrupo
    {
        public string RgrIdStGrupoUsuario { get; set; }
        public string RgrIdStOpcion { get; set; }
        public string RgrStAdicionar { get; set; }
        public string RgrStEliminar { get; set; }
        public string RgrStImprimir { get; set; }
        public string RgrStModificar { get; set; }
        public string RgrStConsultar { get; set; }
        public string RgrIdStUsuario { get; set; }
        public DateTime? RgrDtFechaHora { get; set; }

        public virtual Seggrupousuarios RgrIdStGrupoUsuarioNavigation { get; set; }
        public virtual Segopciones RgrIdStOpcionNavigation { get; set; }
    }
}
