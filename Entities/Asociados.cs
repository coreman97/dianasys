﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Asociados
    {
        public Asociados()
        {
            Empresas = new HashSet<Empresas>();
        }

        public int AflIdInCodigo { get; set; }
        public string AflIdStTipoDocumento { get; set; }
        public string AflIdStIdentificacion { get; set; }
        public string AflStNombreAfiliado { get; set; }
        public string AflStDireccion { get; set; }
        public string AflStTelefono { get; set; }
        public string AflStCelular { get; set; }
        public string AflStCorreoElectronico { get; set; }
        public int AflIdInPaisLocalizacion { get; set; }
        public int AflIdInDptoLocalizacion { get; set; }
        public int AflIdinMunicipioLocalizacion { get; set; }
        public DateTime AflDtFechaAfiliacion { get; set; }
        public string AflIdStEstado { get; set; }
        public DateTime? AflDtFechaRetiro { get; set; }
        public string AflIdStUsuario { get; set; }
        public DateTime? AflDtFechaHora { get; set; }
        public string AflIdStUsuarioAprobo { get; set; }
        public DateTime? AflDtFechaHoraAprobacion { get; set; }

        public virtual Tipodocumentos AflIdStTipoDocumentoNavigation { get; set; }
        public virtual Segusuarios AflIdStUsuarioNavigation { get; set; }
        public virtual ICollection<Empresas> Empresas { get; set; }
    }
}
