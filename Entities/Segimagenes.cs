﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segimagenes
    {
        public Segimagenes()
        {
            Segopciones = new HashSet<Segopciones>();
        }

        public decimal ImgInIdImagen { get; set; }
        public string ImgStDescripcion { get; set; }

        public virtual ICollection<Segopciones> Segopciones { get; set; }
    }
}
