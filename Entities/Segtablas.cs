﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segtablas
    {
        public Segtablas()
        {
            Seghistorial = new HashSet<Seghistorial>();
        }

        public int TabIdInTabla { get; set; }
        public string TabStDescripcion { get; set; }

        public virtual ICollection<Seghistorial> Seghistorial { get; set; }
    }
}
