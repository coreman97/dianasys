﻿namespace DianaSys.Entities
{
    public partial class Conmovimientos
    {
        public int MovIdInConsecutivo { get; set; }
        public int MovIdInAsociado { get; set; }
        public string MovIdStEmpresa { get; set; }
        public string MovIdStComprobante { get; set; }
        public string MovStNumero { get; set; }
        public string MovIdStCuenta { get; set; }
        public string MovIdStSubcuenta { get; set; }
        public string MovIdStAuxiliar { get; set; }
        public string MovIdStAuxiliarUno { get; set; }
        public string MovIdStCentro { get; set; }
        public string MovIdStSubcentro { get; set; }
        public string MovIdStNit { get; set; }
        public string MovStDescripcion { get; set; }
        public decimal MovInValor { get; set; }
        public decimal MovInValorIva { get; set; }
        public decimal MovInIvaMayorValor { get; set; }
        public string MovStCheuqe { get; set; }
        public int MovInItem { get; set; }
    }
}