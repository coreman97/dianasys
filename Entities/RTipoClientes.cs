﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DianaSys.Entities
{
    public partial class RTipoclientes
    {
        public int RtiIdStAsociado { get; set; }
        public string RtiIdStEmpresa { get; set; }
        public string RtiIdStNitCliente { get; set; }
        public string RtiIdStTipoCliente { get; set; }    
    }
}
