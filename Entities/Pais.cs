﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Pais
    {
        public Pais()
        {
            Dpto = new HashSet<Dpto>();
        }

        public int PaiIdInCodigo { get; set; }
        public string PaiStNombre { get; set; }
        public string PaiStCodigoDian { get; set; }
        public string PaiStUsuario { get; set; }
        public DateTime? PaiDtFechaHora { get; set; }

        public virtual ICollection<Dpto> Dpto { get; set; }
    }
}
