﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Contmovimiento
    {
        public int TmoIdInConsecutivo { get; set; }
        public string TmoIdStEmpresa { get; set; }
        public int TmoIdInAsociado { get; set; }
        public string TmoIdStComprobante { get; set; }
        public string TmoStNumero { get; set; }
        public int TmoIdInAno { get; set; }
        public int TmoIdInMes { get; set; }
        public string TmoIdStAplicacion { get; set; }
        public string TmoStDescripcion { get; set; }
        public DateTime TmoDtFecha { get; set; }
        public string TmoIdStUsuario { get; set; }
        public DateTime TmoDtFechaHora { get; set; }

        public virtual Segusuarios TmoIdStUsuarioNavigation { get; set; }
    }
}
