﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Segnivelopcion
    {
        public Segnivelopcion()
        {
            Segopciones = new HashSet<Segopciones>();
        }

        public decimal NopInIdNivelOpcion { get; set; }
        public string NopStDescripcion { get; set; }

        public virtual ICollection<Segopciones> Segopciones { get; set; }
    }
}
