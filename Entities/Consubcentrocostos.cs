﻿using System;
using System.Collections.Generic;

namespace DianaSys.Entities
{
    public partial class Consubcentrocostos
    {
        public int SceIdInAsociado { get; set; }
        public string SceIdStEmpresa { get; set; }
        public string SceIdStCentro { get; set; }
        public string SceIdStCodigo { get; set; }
        public string SceStNombre { get; set; }
        public string SceStEstado { get; set; }
        public string SceIdStUsuario { get; set; }
        public DateTime? SceDtFechaHora { get; set; }
    }
}
