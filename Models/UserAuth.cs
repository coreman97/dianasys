﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DianaSys.Models
{
    public class UserAuth
    {
        public string username { get; set; }
        public string contraseña { get; set; }
        public string empresa { get; set; }
    }
}
